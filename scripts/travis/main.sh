#!/bin/bash

set -ev

./scripts/lint-check.sh
./scripts/travis/get-images.sh
./scripts/travis/run-tests.sh
./scripts/travis/push-images.sh
