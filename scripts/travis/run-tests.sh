#!/bin/bash

set -e

# This script will build and test all modified services or all services if core is modified

# shellcheck source=scripts/travis/get-modified-services.sh
# shellcheck disable=SC1091
source "$(dirname "$0")/get-modified-services.sh"
modified_services=( ${modified_services[@]} )
# shellcheck source=scripts/travis/get-all-services.sh
# shellcheck disable=SC1091
source "$(dirname "$0")/get-all-services.sh"
all_services=( ${all_services[@]} )
# shellcheck source=scripts/travis/utils.sh
# shellcheck disable=SC1091
source "$(dirname "$0")/utils.sh"

IS_INCLUDED="y"

function run_tests {
  service=$1

  if [ "$service" == "core" ]
  then
    return
  fi

  echo "Testing: ${service}"

  REMOVE_LOG=true bash -c "make test service=${service}"
}

# If core is changed all services must be rebuilt
if [ "$(utils::contains "core" "${modified_services[@]}")" == "$IS_INCLUDED" ]; then
  echo "Testing all services: " "${all_services[@]}"
  for service in "${all_services[@]}";
  do
    run_tests $service
  done
else
  echo "Testing modified services: " "${modified_services[@]}"
  for service in "${modified_services[@]}";
  do
    run_tests $service
  done

  # Run e2e tests even if api was not modified
  if [ "$(utils::contains "api" "${modified_services[@]}")" != "$IS_INCLUDED" ]; then
    run_tests api
  fi
fi
