# Docker

## Development workflow

```make dev``` will be your main command for development.

By default ```make dev``` will sync your local files to the container and live reload
the server when changes are detected.

To run commands inside a container you can use ```make bash service=name```, this is
the recommended way to run tests (all services implement a make test command you can use).

You can also use ```make test service=name``` to run tests, but you will need to rebuild the image
each time since this command is intended for CI not for development.
