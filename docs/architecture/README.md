# Architecture

Use this repo as a base to start new projects, it includes everything you should need
including database management examples, testing examples, eslint configurations,
automation (code coverage reports, travis configurations, docker commands, etc).

Refer to [Deployments](/docs/git/README.md) to learn how it all comes together in a
continuous delivery work flow.

TODO: Implement Agave Lab microservice yeoman generator for Node.js and Go.
You can use our microservice generator to add new microservices too.
