# Coding standards

- Make sure your editor support eslint.

## Javascript

- Take full advantage of any ES6 features node.js supports (classes, arrow functions, const, let, yield, etc)
- Avoid callbacks. If and external dependency uses callbacks create a wrapper with promises.
