# Testing

There are 3 testing scopes for services:
- Unit: Test individual components in isolation
- Functional: Black box testing of the service in isolation from external dependencies
- Integration: Test contracts with external services

e2e tests:
Test the whole project as a blackbox through apigateway requests isolated only from third party services.

For project integration tests services need to be able to seed data when indicated with SEED_TEST_DATA.

## Javascript

Backend testing consists of unit and functional testing:

Unit: Using Mocha, Sinon and Chai single functions are isolated and tested against all possible scenarios to ensure a correct behavior.
Functional: Using Mocha, Superagent, Chai and is-my-json-valid we test features of our product from the point of view of the user (in this case whomever is making http requests to our API).

### Project structure

All our backend projects include a “test” directory with the same structure as this repo.

Both unit and functional directories include fixtures, factories and helpers directories when needed.

Factories are classes that create objects for you.

Fixtures are files with static data used to test different scenarios, for example when testing a login function we want to test both a valid login and an invalid one so we can have a fixture for each. Fixtures rely on factories
to build different objects.

Helpers are functions that are not directly related to a test but are needed to make the test, for example authenticating before testing a feature that requires a logged user or cleaning up the database after the tests.

### Architecture

In order to make sequelize models testing much easier we need to separate the model definition from its functionality. That’s why we have a models/ directory and model_functions/ directory, we don’t want to worry about sequelize dependencies so we focus on testing the model’s functions by isolating them from sequelize definitions.

### Unit

Our goal is to isolate each layer of our project and make sure they work as expected in all possible scenarios, this means testing controllers without actually communicating with real models and testing models without communicating with a real database connection.

We rely on test doubles like spies, stubs and fakes to isolate individual functions and get them in whatever specific conditions we desire to test against.

Analyzing each function to determine all possible scenarios and the desired result for each of them is how we start writing our test suites for each controller and model in our project.

### Functional

Here our goal is to interact with our API the same way a client could (the web or mobile app) and make sure it behaves as expected.

Test suites are designed for a complete feature like Login or Register, same as unit testing we need to analyze all possible scenarios like a client not providing correct data and make a test for each of them making sure we return the expected response.

We use is-my-json-valid to clearly define response schemas for our end-points and test our API responses against them.

### Resources

https://mochajs.org
https://github.com/chaijs/chai
https://github.com/mafintosh/is-my-json-valid
http://json-schema.org
https://github.com/visionmedia/superagent
http://sinonjs.org
