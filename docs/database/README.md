# Database

## Javascript

### Postgres

The folder contrains the `create-db.sh` script, this will create a development and test database if it's required for the microservice.

In order to make it work on the microservice, it would require 4 enviroment variables be defined on the environment: `$POSTGRES_USER`, `$POSTGRES_PASSWORD`, `DB_HOST` and `$DB_NAME`.

Also the Dockerfile should be updated to include psql and netcat in the container adding the following line:

```Docker
RUN apt-get update && apt-get install -y postgresql netcat
```

And the final step is to include the `core/postgres/create-db.sh` script in the `startup.dev` script just by doing:

```bash
source /var/lib/core/postgres/create-db.sh
```

### Add Sequelize as ORM of the service


First it would be required to install sequelize and sequelize-cli in the project, this can be accomplished through the follwoing command:


```bash
$ npm install --save sequelize@2.0.0-rc1 sequelize-cli
```

Once the packages are installed sequelize-cli can be initialized running:

```bash
$ node_modules/.bin/sequelize init
```

This will create 4 folders:

* `config/` in this folder we would find a `config.json` file, that we would replace to be a `config.js` file.
* `migrations/` and `seeders/` this folders are straight forward about their name.
* `models/` by default an `index.js` file is built, in order to work with the docker structure the lines 7 and 8 will need to be modified.

From this:
```javascript
var fs        = require('fs');
var path      = require('path');
var Sequelize = require('sequelize');
var basename  = path.basename(module.filename);
var env       = process.env.NODE_ENV || 'development';
var config    = require(__dirname + '/../config/config.json')[env];
var db        = {};
```

To:
```javascript
var fs        = require('fs');
var path      = require('path');
var Sequelize = require('sequelize');
var basename  = path.basename(module.filename);
var config    = require(__dirname + '/../config/config');
var db        = {};
```

Once this is changed, the `config.js` would require the following structure and information.

```javascript
const env = process.env.NODE_ENV || 'development';
let database = process.env.DB_NAME;
let logging = env === 'test' ? false : true;

if (env === 'test' || env === 'development') {
  database = env === 'test' ? database + '_test' : database + '_dev';
}

const config = {
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: database,
  host: process.env.DB_HOST,
  dialect: "postgres",
  logging: logging
}

module.exports = config;
```

For the last step in establishing a connection and since this example is based to work with postgresql, we can install the missing packages with:

```bash
$ npm install --save pg pg-hstore
```

In order to make this implementation keep up with changes, the following lines would be required to be added either in the startup script or wherever is more convenient:

```bash
./node_modules/.bin/sequelize db:migrate --config config/config.js
./node_modules/.bin/sequelize db:seed --config config/config.js
```

## e2e front end tests

In some cases the front end will need the backend to seed a new database with test data. Add this in /scripts using fixtures and helpers to load data similar to functional tests.

## Integration test

When developing a service it might require to interact with another service, for this integration tests are necessary but in order to have successful scenarios the application might expect some seed in the database.
This can be acomplished using a `startup.integration.sh` script that will allow us to run specific commands also we will need to extend the `docker-compose-dev.yml` file to let the service know which entrypoint to use.

Startup script:

```bash
node /integration_seeds/oauth.js
```

Docker compose:

```Docker
version: '2'

services:
  user:
    entrypoint: /home/docker/scripts/startup.integration.sh
  log:
    entrypoint: /home/docker/scripts/startup.integration.sh
```

Seeder example:

```javascript
const { User } = require('../models');

var data = [{
  email: 'admin@email.com',
  password: '123456'
}];

User.bulkCreate(data);
```

**NOTE:** Also if the primary key is accesible through the parameters that the seed require the method `upsert` can be used instead, this will avoid creating junk data in the database.
