# Dependencies management

- Use external libraries only if you actually are going to use most features, if not, add just what you need.
- Dependency version monitoring. Keep track of your dependencies and update when possible.

## Javascript

- Use npm shrinkwrap or yarn.lock to lock dependencies.
