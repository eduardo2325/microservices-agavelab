# API design

### Internal communication

Internally all services use GRPC or AMQP to communicate to keep it simple and efficient.
Additionally the API gateway needs to expose a RESTful interface for clients that can't
use GRPC.

### Restful endpoints for Gateway

Follow the standard RESTful specification.

Use HTTP status codes to indicate the outcome (200, 400, 404, 500).
Use HTTP verbs GET, POST, PUT and DELETE.

### Responses

All endpoint responses should use the following structure:

```
{
  type: 'User',
  data: {
    name: 'user'
  }
}
{
  type: 'Users',
  data: [
    {
      name: user
    }
  ]
}
```

### Versioning

Use semantic versioning for each service.

To be defined:
  - Versions inside src/ folder of each microservice
  - New folder for each new major version
