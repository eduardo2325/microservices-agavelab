# Security

### Authentication

For most projects the oauth2 microservice used by the API Gateway implemented in this repo is enough.

https://oauth.net/2/

### Secret Management
We use [torus](https://www.torus.sh/) to provision both build and run time secrets to our containers.

If ever in doubt, navigate torus' CLI for command usage our checkout their [outdated docs](https://www.torus.sh/docs/latest/start-here).

####Buildtime
There are a number of secrets required for a successful travis build, these range from API Tokens (github, codecov) to docker credentials for pulling and pushing images. If baking a secret into a docker image, it is of utmost importance to make sure that the pushed image is correctly secured (i.e. private) on the registry, given that anyone with access to the image can access its buildtime secrets through the docker inspect command.

In order to setup machine access to secrets in torus the following must be done:

* Install torus and signup in your development environment.
* On the project's root make sure the `.torus.json` file is present then run `torus link` from that folder. This will set the default organization and project to your torus session and will save some typing later.
* From here on, you'll either need an invite to the organization to be able to access its secrets, or you can just follow through with your own.
* Create or inform yourself about the available environments with `torus envs list`.
* Once you've determined the environment, do the same for torus services, which are another way of segregating credentials regarding their nature, this is done with `torus services list` or `torus services create`.
* Set some secrets on a determined torus environment and service so: `torus set -e <env-name> -s <service-name> <secret-name> <value>` for example: `torus set -e travis -s travis GITHUB_TOKEN sosecret`.
* You'll see output similar to this one:

		Credentials retrieved
		Keypairs retrieved
		Encrypting key retrieved
		Credential encrypted
		Completed Operation
		
		Credential key has been set at /agavelab/backend-base/travis/travis/*/*/github_token

* You'll need to set up a torus machine so: `torus machines create -o <your-org>` this will generate new access tokens and create the machine on torus' backend. The output should look like this:
		
		✔ Org name: agavelab
		✔ Create a new role: read-write
		✔ Enter machine name: read-write-machine

		Machine role read-write created for org agavelab.

		Creating machine "read-write-machine"
		Generating machine token
		Generating token keypairs
		Uploading token keypairs
		Creating keyring memberships for token
		Uploading keyring memberships
		Machine created

		You will only be shown the secret once, please keep it safe.

		Machine ID:           04bu6u[redacted]jqrtdpc
		Machine Token ID:     04c40[redacted]qundw
		Machine Token Secret: trCSNU[redacted]xk6hK
* Your can create a new role or specify an existing one, just make sure that you follow the least privilege principle by giving the machine access only to the necessary secrets.
* Machine roles are analogous to teams for torus users. Giving a machine access to secrets means that one must set permissions to that machine's role and subsequently all machines with such role will be able to access those secrets.
* To set permissions for a role (or team for that matter) just use the following command syntax:
	
		torus allow <crudl> <path> <team|machine-role> 
* For example, to give our new machine **read write** access to **just** our previously defined secret we would run:
	
		torus allow rul /agavelab/backend-base/travis/travis/*/*/github_token read-write
			
* This would allow **read update** and **list** access to all machines that belong to the **read-write** role.
* Consequently, the following command would grant the same permissions to the same role with the exception of granting access to **all** secrets on that path:

		torus allow rul /agavelab/backend-base/travis/travis/*/*/* read-write
* Once this is done, just export `TORUS_TOKEN_SECRET`and `TORUS_TOKEN_ID` in travis, and wrap the commands that require such secrets with `torus run [command options] [--] <command> [<arguments>...]`. These will be available as environment variables.



####Runtime
TODO