# Recommended practices

* [Database](/docs/database/README.md)
* [Testing](/docs/testing/README.md)
* [Git](/docs/git/README.md)
* [Architecture](/docs/architecture/README.md)
* [Code standards](/docs/code/README.md)
* [Dependencies management](/docs/dependencies/README.md)
* [API design](/docs/api/README.md)
* [Security](/docs/security/README.md)
* [Deployments](/docs/deployment/README.md)
* [Docker](/docs/docker/README.md)
