#!/bin/bash

set -x -u

torus run -e "$TORUS_ENV" -s "$TORUS_SERVICE" --org agave-lab --project backend-base -training-backend -- node app.js
