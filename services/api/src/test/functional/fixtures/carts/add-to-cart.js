const _ = require('lodash');
const cartItemFixtures = require('/var/lib/core/integration_fixtures/cartItem');
const cartFactory = require('../../factories/cart');

const FixturesHelper = require('../helpers/fixtures-helper');

class addToCart extends FixturesHelper {
    constructor() {
        const data = {
            product_id: 1,
            quantity: 1
        }
        super(data);

    }

    validAdditionToCart() {
        return this.data;
    }

    validDiscountAddition(){
        return _.find(cartItemFixtures, { product_id: 2, quantity: 5});
    }
    
    validDiscountSummary() {
    
        return {
            products: [
                {
                    name: 'T-Shirt',
                    quantity: 5,
                    individualTotal: 95
                }
            ],
            totalQuantity: 5,
            totalPrice: 95
        };
    }

    notExists(){
        const data = this.data;

        data.product_id = 10;

        return data;
    }
}

module.exports = new addToCart();
