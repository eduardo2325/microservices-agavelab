const listsFactory = require('../../factories/lists');

class ListsFixtures {
  constructor(limit = 1000, page = 1) {
    this.limit = limit;
    this.pageNumber = page;
  }

  allElements() {
    return listsFactory.orderBy(this.pageNumber, 0);
  }
  defaultOrder() {
    return listsFactory.orderBy(this.pageNumber);
  }
  defaultOrderDesc() {
    return listsFactory.orderBy(this.pageNumber, this.limit, undefined, true);
  }
  invalidOrderBy() {
    return listsFactory.orderBy(this.pageNumber, this.limit, 'invalid');
  }
  invalidPage(attr) {
    return listsFactory.orderBy(attr, this.limit, 'invalid');
  }
  invalidLimit(attr) {
    return listsFactory.orderBy(this.pageNumber, attr, 'invalid');
  }
  invalidOrderDesc(attr) {
    return listsFactory.orderBy(this.pageNumber, this.limit, attr, 'invalid');
  }
  orderBy(attr, desc) {
    return listsFactory.orderBy(this.pageNumber, this.limit, attr, desc);
  }
  pageSize() {
    const pageSize = this.limit;

    return listsFactory.pageSize(pageSize);
  }
  withoutPageSize() {
    return listsFactory.pageSize(0);
  }
  pageOffset() {
    const pageSize = this.limit;
    const page = this.pageNumber;

    return listsFactory.pageSize(pageSize, page);
  }
  pageCounter(totalRows) {
    const pageSize = this.limit;

    return {
      pageSize,
      pages: Math.ceil(totalRows / pageSize),
      total: totalRows
    };
  }
  pageCounterOffset(totalRows) {
    const pageSize = this.limit;
    const page = this.pageNumber || 1;

    return {
      pageSize,
      page,
      startElement: page * pageSize - pageSize,
      lastElement: (page + 1) * pageSize - pageSize,
      pages: Math.ceil(totalRows / pageSize),
      total: totalRows
    };
  }
}

module.exports = ListsFixtures;
