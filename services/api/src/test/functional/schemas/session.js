const loginSchema = {
  required: true,
  type: 'object',
  properties: {
    user_id: {
      type: 'integer',
      requried: true
    },
    token: {
      type: 'string',
      required: true
    },
    email: {
      type: 'string',
      required: true
    },
    role: {
      type: 'string',
      required: true
    },
    expiration_date: {
      type: 'string',
      required: true
    }
  },
  additionalProperties: false
};

module.exports = loginSchema;
