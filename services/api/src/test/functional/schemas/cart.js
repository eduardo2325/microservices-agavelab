const cartSchema = {
    required: true,
    type: 'object',
    properties: {
        products: {
            required: true,
            type: 'array',
            minItems: 1,
            product: {
                required: true,
                type: 'object',
                properties: {
                    name: {
                        required: true,
                        type: 'string'
                    },
                    quantity: {
                        required: true,
                        type: 'integer'
                    },
                    individualTotal: {
                        required: true,
                        type: 'number'
                    }
                }
            }
        },
        totalQuantity: {
            required: true,
            type: 'integer'
        },
        totalPrice: {
            required: true,
            type: 'number'
        }
    },
    additionalProperties: false
  };
  
  module.exports = cartSchema;
  