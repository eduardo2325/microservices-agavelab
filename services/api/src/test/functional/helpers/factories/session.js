const formatHelper = require('../formatHelpers');

class SessionFactory {
  login(params) {
    const { email, password } = params;
    const login = {
      email: formatHelper.formatStringValue(email, 'email'),
      password: formatHelper.formatStringValue(password, 'password')
    };

    return {
      query: `
        mutation {
          login(input: {
            ${login.email}
            ${login.password}
          }) {
            user_id,
            token,
            role,
            expiration_date,
            email
          }
        }
      `
    };
  }

  logout() {
    return {
      query: `
        mutation {
          logout {
            success
          }
        }
      `
    };
  }

  me() {
    return {
      query: `
        query{
          me{
            user_id,
            token,
            role,
            expiration_date,
            email
          }
        }
      `
    };
  }
}

module.exports = new SessionFactory();
