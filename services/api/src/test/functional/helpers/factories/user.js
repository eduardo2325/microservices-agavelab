const formatHelper = require('../formatHelpers');

class UserFactory {
  updatePassword(params) {
    const { old_password, new_password, confirmation_password } = params;
    const update = {
      old_password: formatHelper.formatStringValue(old_password, 'old_password'),
      new_password: formatHelper.formatStringValue(new_password, 'new_password'),
      confirmation_password: formatHelper.formatStringValue(confirmation_password, 'confirmation_password')
    };

    return {
      query: `
        mutation {
          updatePassword(input: {
            ${update.old_password}
            ${update.new_password}
            ${update.confirmation_password}
          }) {
            id,
            first_name,
            last_name,
            email,
            role,
            verified
          }
        }
      `
    };
  }
}

module.exports = new UserFactory();
