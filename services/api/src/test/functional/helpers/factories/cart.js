const formatHelper = require('../formatHelpers');

class CartFactory {
  addToCart(params) {
    const { product_id, quantity } = params;
    const cart = {
      product_id: formatHelper.formatNumericValue(product_id, 'product_id'),
      quantity: formatHelper.formatNumericValue(quantity, 'quantity')
    };
    
    return {
       query:  `mutation{
            addToCart(input:{${cart.product_id}${cart.quantity}}){
                products{
                    name
                    quantity
                    individualTotal
                }
                totalQuantity
                totalPrice
            }
        }`
    };
  }

}

module.exports = new CartFactory();
