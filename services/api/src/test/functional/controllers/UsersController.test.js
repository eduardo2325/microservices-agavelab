const _ = require('lodash');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const API = require('../helpers/api');
const validate = require('../helpers/validate');

const errorCodes = require('../../../config/error-codes');

const errorSchema = require('../schemas/error');

chai.use(chaiAsPromised);

describe('functional/Users controller', () => {

  describe('updatePassword', () => {
    const userFixtures = require('../fixtures/users/update-password');

    const fixtures = require('/var/lib/core/integration_fixtures/user');
    const user = _.find(fixtures, { email: 'update_password@email.com' });
    const { email, password } = user;

    beforeEach(() => {
      return API.login({ email, password });
    });

    it('should return error if there is no active session', () => {
      const data = userFixtures.validPassword();

      return API.logout()
        .then(() => API.updatePassword(data))
        .should.be.fulfilled
        .then(response => {

          const errorData = response.data.errors[0];

          response.status.should.be.eql(200);
          errorData.code.should.be.eql(errorCodes.Session.Unauthorized);
          return validate(errorSchema)(errorData);
        });
    });

    it('should return error if the old password is not provided', () => {
      const data = userFixtures.withoutField('old_password');

      return API.updatePassword(data)
        .should.be.rejected
        .then(response => {

          const { error } = response.response;

          response.status.should.be.eql(400);
          return error.path.should.be.eql('/api/v1/graphql');
        });
    });

    it('should return error if the old password is provided as an empty string', () => {
      const data = userFixtures.emptyField('old_password');

      return API.updatePassword(data)
        .should.be.fulfilled
        .then(response => {

          const errorData = response.data.errors[0];

          response.status.should.be.eql(200);
          errorData.path.should.be.eql('old_password');
          errorData.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData);
        });
    });

    it('should return error if the new password is not provided', () => {
      const data = userFixtures.withoutField('new_password');

      return API.updatePassword(data)
        .should.be.rejected
        .then(response => {

          const { error } = response.response;

          response.status.should.be.eql(400);
          return error.path.should.be.eql('/api/v1/graphql');
        });
    });

    it('should return error if the new password is provided as an empty string', () => {
      const data = userFixtures.emptyField('new_password');

      return API.updatePassword(data)
        .should.be.fulfilled
        .then(response => {

          const errorData = response.data.errors[0];

          response.status.should.be.eql(200);
          errorData.path.should.be.eql('new_password');
          errorData.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData);
        });
    });

    it('should return error if the confirmation password is not provided', () => {
      const data = userFixtures.withoutField('confirmation_password');

      return API.updatePassword(data)
        .should.be.rejected
        .then(response => {

          const { error } = response.response;

          response.status.should.be.eql(400);
          return error.path.should.be.eql('/api/v1/graphql');
        });
    });

    it('should return error if the confirmation password is provided as an empty string', () => {
      const data = userFixtures.emptyField('confirmation_password');

      return API.updatePassword(data)
        .should.be.fulfilled
        .then(response => {

          const errorData = response.data.errors[0];

          response.status.should.be.eql(200);
          errorData.path.should.be.eql('confirmation_password');
          errorData.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData);
        });
    });

    it('should return error if the old password is incorrect', () => {
      const data = userFixtures.wrongOldPassword();

      return API.updatePassword(data)
        .should.be.fulfilled
        .then(response => {

          const errorData = response.data.errors[0];

          response.status.should.be.eql(200);
          errorData.path.should.be.eql('old_password');
          errorData.code.should.be.eql(errorCodes.User['old_password is incorrect']);
          return validate(errorSchema)(errorData);
        });
    });

    it('should return error if the confirmation password is incorrect', () => {
      const data = userFixtures.wrongConfirmationPassword();

      return API.updatePassword(data)
        .should.be.fulfilled
        .then(response => {

          const errorData = response.data.errors[0];

          response.status.should.be.eql(200);
          errorData.path.should.be.eql('confirmation_password');
          errorData.code.should.be.eql(errorCodes.User['confirmation_password is different']);
          return validate(errorSchema)(errorData);
        });
    });

    it('should return success if there was no issue', () => {
      const userSchema = require('../schemas/user');
      const data = userFixtures.validPassword();

      return API.updatePassword(data)
        .should.be.fulfilled
        .then(response => {

          const { updatePassword } = response.data.data;

          response.status.should.be.eql(200);
          return validate(userSchema)(updatePassword);
        });
    });
  });
});
