const _ = require('lodash');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const API = require('../helpers/api');
const validate = require('../helpers/validate');

const errorCodes = require('../../../config/error-codes');

const errorSchema = require('../schemas/error');

chai.use(chaiAsPromised);

describe.only('functional/Carts controller', () => {

  describe('addToCart', () => {
    const cartFixtures = require('../fixtures/carts/add-to-cart');

    beforeEach(() => {
      if(!API.isLoggedIn()){
        return API.adminLogin();
      }
      return Promise.resolve();
    });
    
    it('should return error if not authenticated and will not allow to add products', function() {
      const data = cartFixtures.validAdditionToCart();

      return API.logout()
        .then(() => API.addToCart(data))
        .should.be.fulfilled
        .then(response => {
          // console.log(response);
          const errorData = response.data.errors[0];
          // console.log(errorData);
          
          response.status.should.be.eql(200);
          errorData.code.should.be.eql(errorCodes.Session.Unauthorized);
          return validate(errorSchema)(errorData);

        })

    });

    it('should return error if product_id is not provided', function() {
      const data = cartFixtures.withoutField('product_id');

      return API.addToCart(data)
        .should.be.rejected
        .then(response => {
          const { error } = response.response;

          response.status.should.be.eql(400);
          return error.path.should.be.eql('/api/v1/graphql');
        });
    
    });

    it('should return error if quantity is not provided', function() {
      const data = cartFixtures.withoutField('quantity');

      return API.addToCart(data)
        .should.be.rejected
        .then(response => {
          const { error } = response.response;

          response.status.should.be.eql(400);
          return error.path.should.be.eql('/api/v1/graphql');
        });
    
    });

    it('should return a valid discount price for T-Shirts', () => {
      const data = cartFixtures.validDiscountAddition();
      const validSummary = cartFixtures.validDiscountSummary();

      return API.addToCart(data)
        .should.be.fulfilled
        .then(response => {

          const { addToCart } = response.data.data;
          addToCart.should.be.deep.eql(validSummary);
          response.status.should.be.eql(200);
        });
    });

    it('should return success if there was no issue', () => {
      const cartSchema = require('../schemas/cart');
      const data = cartFixtures.validAdditionToCart();

      return API.addToCart(data)
        .should.be.fulfilled
        .then(response => {

          const { addToCart } = response.data.data;

          response.status.should.be.eql(200);
          return validate(cartSchema)(addToCart);
        });
    });

    it('should return error if product does not exist', () => {
      const data = cartFixtures.notExists();

      return API.addToCart(data)
        .should.be.fulfilled
        .then(response => {
          const errorData = response.data.errors[0];
          response.status.should.be.eql(200);
          errorData.path.should.be.eql('SequelizeForeignKeyConstraintError');
          errorData.code.should.be.eql(errorCodes.Server.Unassigned);

        });
    });
    
  });
});
