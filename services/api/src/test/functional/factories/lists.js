class ListsFactory {

  orderBy(page, limit, order_by, order_desc = false) {
    return {
      page,
      limit,
      order_by,
      order_desc
    };
  }

  pageSize(limit, page) {
    return {
      limit,
      page
    };
  }
}

module.exports = new ListsFactory();

