class UserFactory {

  updatePassword(old_password, new_password) {
    return {
      old_password,
      new_password,
      confirmation_password: new_password
    };
  }
}

module.exports = new UserFactory();

