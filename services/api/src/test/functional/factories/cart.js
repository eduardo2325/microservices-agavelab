class CartFactory {

    addToCart(product_id, quantity) {
      return {
        product_id,
        quantity
      };
    }
  }
  
  module.exports = new CartFactory();
  
  