const { request, errors } = require('../../../../common/fixtures/');
const { guid, responseType } = request;
const sendError = errors.customError('User');
const input = {
  old_password: 'old password',
  new_password: 'new password',
  confirmation_password: 'new password'
};
const user = {
  id: 1,
  first_name: 'First name',
  last_name: 'Last name',
  email: 'some@email.com',
  role: 'ADMIN',
  verified: true
};
const params = {
  guid,
  user_id: 1,
  old_password: input.old_password,
  new_password: input.new_password
};

module.exports = {
  context: {
    guid,
    user: {
      user_id: 1
    }
  },
  args: {
    input
  },
  argsWrongConfirmation: {
    input: {
      old_password: 'old password',
      new_password: 'new password',
      confirmation_password: 'wrong cofirmation password'
    }
  },
  sendParams: [
    'user',
    'updatePassword',
    params
  ],
  response: user,
  logParams: [
    'Update password',
    user,
    responseType,
    guid
  ],
  error: sendError,
  errorHelperParams: [
    'User',
    sendError,
    guid
  ],
  errorHelperParamsWrongConfirmation: [
    'User',
    {
      path: 'confirmation_password',
      message: 'confirmation_password is different'
    },
    guid
  ]
};
