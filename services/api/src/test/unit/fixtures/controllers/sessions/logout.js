const { request, errors } = require('../../../../common/fixtures/');
const { guid, responseType } = request;
const sendError = errors.customError('Session');
const token = 'token';

module.exports = {
  context: {
    guid,
    user: {
      token: 'token'
    }
  },
  args: { },
  sendParams: [
    'session',
    'deleteToken',
    { guid, token }
  ],
  response: {
    success: true
  },
  logParams: [
    'Session logout',
    {
      success: true
    },
    responseType,
    guid
  ],
  error: sendError,
  errorHelperParams: [
    'Session',
    sendError,
    guid
  ]
};
