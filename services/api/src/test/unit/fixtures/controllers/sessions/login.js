const { request, errors } = require('../../../../common/fixtures/');
const { guid, responseType } = request;
const sendError = errors.customError('Session');
const input = {
  email: 'some@email.com',
  password: 'some password'
};
const user = {
  id: 1,
  first_name: 'A name',
  last_name: 'A last name',
  email: 'some@email.com',
  role: 'ADMIN',
  verified: true
};

module.exports = {
  context: {
    guid
  },
  args: {
    input
  },
  sendParams: [
    'session',
    'create',
    { guid, ...input }
  ],
  response: user,
  logParams: [
    'Session login',
    user,
    responseType,
    guid
  ],
  error: sendError,
  errorHelperParams: [
    'Session',
    sendError,
    guid
  ]
};
