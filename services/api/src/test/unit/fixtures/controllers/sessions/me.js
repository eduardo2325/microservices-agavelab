const { request, errors } = require('../../../../common/fixtures/');
const { guid, responseType } = request;
const sendError = errors.customError('Session');
const token = 'token';
const user = {
  id: 1,
  first_name: 'A name',
  last_name: 'A last name',
  email: 'some@email.com',
  role: 'ADMIN',
  verified: true
};

module.exports = {
  context: {
    guid,
    user: {
      token: 'token'
    }
  },
  args: { },
  sendParams: [
    'session',
    'me',
    { guid, token }
  ],
  response: user,
  logParams: [
    'Me',
    user,
    responseType,
    guid
  ],
  error: sendError,
  errorHelperParams: [
    'Session',
    sendError,
    guid
  ]
};
