const { request, errors } = require('../../../../common/fixtures/');
const { guid, responseType } = request;
const sendError = errors.customError('Cart');
const logMessageError = errors.logMessageError;
const input = {
  product_id: 2,
  quantity: 5
};
const summary = {
    products: [
        {
            name: 'T-Shirt',
            quantity: 5,
            individualTotal: 95
        }
    ],
    totalQuantity: 5,
    totalPrice: 95
};
const params = {
    guid,
    user_id: 1,
    product_id: 2,
    quantity: 5
}

module.exports = {
  context: {
    guid,
    user: {
        user_id: 1
    }
  },
  args: {
    input
  },
  sendParams: [
    'cart',
    'addToCart',
    { guid, ...params }
  ],
  response: summary,
  logParams: [
    'Add product to cart',
    summary,
    responseType,
    guid
  ],
  error: sendError,
  errorHelperParams: [
    'cart',
    sendError,
    guid
  ],
  logMessageError: [
    'cart',
    logMessageError,
    guid
  ]
};
