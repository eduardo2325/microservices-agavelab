const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const helper = require('../../../api/helpers/error');
const helperFixtures = require('../fixtures/helpers/error');

const log = require('/var/lib/core/js/log').prototype;

chai.should();

describe('unit/Error helper', () => {

  afterEach(() => {
    sandbox.restore();
  });

  describe('format', () => {

    const fixtures = helperFixtures.format;
    const { invalidRequestParams, notFoundParams, unassignedTypeParams, unassignedMsgParams, existingCodeParams,
      invalidRequestError, notFoundError, unassignedTypeError, unassignedMsgError, existingCodeError } = fixtures;

    it('should return invalid request error code', () => {
      return Promise.resolve()
        .then(() => {
          const error = helper.format.apply(helper, invalidRequestParams);

          error.should.be.eql(invalidRequestError);
        });
    });

    it('should return not found error code', () => {
      return Promise.resolve()
        .then(() => {
          const error = helper.format.apply(helper, notFoundParams);

          error.should.be.eql(notFoundError);
        });
    });

    it('should return unassigned error code if type in error dictionary', () => {
      return Promise.resolve()
        .then(() => {
          const error = helper.format.apply(helper, unassignedTypeParams);

          error.should.be.eql(unassignedTypeError);
        });
    });

    it('should return unassigned error code if message not in type', () => {
      return Promise.resolve()
        .then(() => {
          const error = helper.format.apply(helper, unassignedMsgParams);

          error.should.be.eql(unassignedMsgError);
        });
    });

    it('should return error code corresponding to type and message', () => {
      return Promise.resolve()
        .then(() => {
          const error = helper.format.apply(helper, existingCodeParams);

          error.should.be.eql(existingCodeError);
        });
    });
  });

  describe('handleResponse', () => {

    const fixtures = helperFixtures.handleResponse;
    const { type, error, guid, formattedError } = fixtures;

    before(() => {
      sandbox.stub(helper, 'format').callsFake(() => formattedError);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should format error and create response', () => {
      return helper.handleResponse(type, error, guid)
        .should.be.rejected
        .then(() => {
          helper.format.calledOnce.should.be.true;
          helper.format.calledWithExactly(type, error).should.be.true;
          log.error.calledOnce.should.be.true;
          log.error.calledWithExactly(error, guid, formattedError).should.be.true;
        });
    });
  });
});
