const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const log = require('/var/lib/core/js/log').prototype;
const controller = require('../../../api/controllers/SessionsController');
const gateway = require('../../../api/helpers/gateway');
const errorHelper = require('../../../api/helpers/error');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Sessions controller', () => {

  afterEach(() => {
    sandbox.restore();
  });

  describe('login', () => {
    const fixtures = require('../fixtures/controllers/sessions/login');
    const { context, args, sendParams, response, logParams, error, errorHelperParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(gateway, 'sendUser').callsFake(() => Promise.resolve(response));
      sandbox.stub(errorHelper, 'handleResponse').callsFake(() => Promise.reject());
    });

    it('should return an error if gateway.sendUser return a reject', () => {
      gateway.sendUser.restore();
      sandbox.stub(gateway, 'sendUser').callsFake(() => Promise.reject(error));

      return controller.login(null, args, context)
        .should.be.rejected
        .then(() => {
          gateway.sendUser.calledOnce.should.be.true;
          gateway.sendUser.args[0].should.eql(sendParams);
          log.message.called.should.be.false;
          errorHelper.handleResponse.calledOnce.should.be.true;
          errorHelper.handleResponse.args[0].should.be.eql(errorHelperParams);
        });
    });

    it('should return a success if there was no issue', () => {

      return controller.login(null, args, context)
        .should.be.fulfilled
        .then(result => {

          gateway.sendUser.calledOnce.should.be.true;
          gateway.sendUser.args[0].should.eql(sendParams);
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logParams);
          errorHelper.handleResponse.called.should.be.false;

          result.should.be.eql(response);
        });
    });
  });

  describe('logout', () => {
    const fixtures = require('../fixtures/controllers/sessions/logout');
    const { context, args, sendParams, response, logParams, error, errorHelperParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(gateway, 'sendUser').callsFake(() => Promise.resolve(response));
      sandbox.stub(errorHelper, 'handleResponse').callsFake(() => Promise.reject());
    });

    it('should return an error if gateway.sendUser return a reject', () => {
      gateway.sendUser.restore();
      sandbox.stub(gateway, 'sendUser').callsFake(() => Promise.reject(error));

      return controller.logout(null, args, context)
        .should.be.rejected
        .then(() => {
          gateway.sendUser.calledOnce.should.be.true;
          gateway.sendUser.args[0].should.eql(sendParams);
          log.message.called.should.be.false;
          errorHelper.handleResponse.calledOnce.should.be.true;
          errorHelper.handleResponse.args[0].should.be.eql(errorHelperParams);
        });
    });

    it('should return a success if there was no issue', () => {

      return controller.logout(null, args, context)
        .should.be.fulfilled
        .then(result => {

          gateway.sendUser.calledOnce.should.be.true;
          gateway.sendUser.args[0].should.eql(sendParams);
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logParams);
          errorHelper.handleResponse.called.should.be.false;

          result.should.be.eql(response);
        });
    });
  });

  describe('me', () => {
    const fixtures = require('../fixtures/controllers/sessions/me');
    const { context, args, sendParams, response, logParams, error, errorHelperParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(gateway, 'sendUser').callsFake(() => Promise.resolve(response));
      sandbox.stub(errorHelper, 'handleResponse').callsFake(() => Promise.reject());
    });

    it('should return an error if gateway.sendUser return a reject', () => {
      gateway.sendUser.restore();
      sandbox.stub(gateway, 'sendUser').callsFake(() => Promise.reject(error));

      return controller.me(null, args, context)
        .should.be.rejected
        .then(() => {
          gateway.sendUser.calledOnce.should.be.true;
          gateway.sendUser.args[0].should.eql(sendParams);
          log.message.called.should.be.false;
          errorHelper.handleResponse.calledOnce.should.be.true;
          errorHelper.handleResponse.args[0].should.be.eql(errorHelperParams);
        });
    });

    it('should return a success if there was no issue', () => {

      return controller.me(null, args, context)
        .should.be.fulfilled
        .then(result => {

          gateway.sendUser.calledOnce.should.be.true;
          gateway.sendUser.args[0].should.eql(sendParams);
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logParams);
          errorHelper.handleResponse.called.should.be.false;

          result.should.be.eql(response);
        });
    });
  });
});
