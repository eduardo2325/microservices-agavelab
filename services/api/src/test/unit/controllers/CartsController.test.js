const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const log = require('/var/lib/core/js/log').prototype;
const controller = require('../../../api/controllers/CartsController');
const gateway = require('../../../api/helpers/gateway');
const errorHelper = require('../../../api/helpers/error');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Carts controller', () => {

  afterEach(() => {
    sandbox.restore();
  });

  describe('addToCart', () => {
    const fixtures = require('../fixtures/controllers/carts/add-to-cart');
    const { context, args, sendParams, response, logParams, error, errorHelperParams, logMessageError } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').returns(true);
      sandbox.stub(gateway, 'sendUser').resolves(response);
      sandbox.stub(errorHelper, 'handleResponse').rejects();
    });

    it('should return an error if gateway.sendUser return a reject', () => {
      gateway.sendUser.restore();
      sandbox.stub(gateway, 'sendUser').callsFake(() => Promise.reject(error));

      return controller.addToCart(null, args, context)
        .should.be.rejected
        .then(() => {
          gateway.sendUser.calledOnce.should.be.true;
          gateway.sendUser.args[0].should.eql(sendParams);
          log.message.called.should.be.false;
          errorHelper.handleResponse.calledOnce.should.be.true;
          errorHelper.handleResponse.args[0].should.be.eql(errorHelperParams);
        });
    });

    it('should return error if log.message returns an exception', () => {
        log.message.restore();
        sandbox.stub(log, 'message').throws(logMessageError);
        // sandbox.stub(log, 'message').rejects(logMessageError);


        return controller.addToCart(null, args, context)
          .should.be.rejected
          .then((result) => {
            gateway.sendUser.calledOnce.should.be.true;
            gateway.sendUser.args[0].should.eql(sendParams);
            log.message.called.should.be.true;
            log.message.threw(logMessageError);
            errorHelper.handleResponse.calledOnce.should.be.true;
        });
    });

    it('should return a success if there was no issue', () => {

      return controller.addToCart(null, args, context)
        .should.be.fulfilled
        .then(result => {

          gateway.sendUser.calledOnce.should.be.true;
          gateway.sendUser.args[0].should.eql(sendParams);
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logParams);
          errorHelper.handleResponse.called.should.be.false;

          result.should.be.eql(response);
        });
    });
  });
  
});
