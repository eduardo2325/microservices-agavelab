const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const log = require('/var/lib/core/js/log').prototype;
const controller = require('../../../api/controllers/UsersController');
const gateway = require('../../../api/helpers/gateway');
const errorHelper = require('../../../api/helpers/error');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Users controller', () => {

  afterEach(() => {
    sandbox.restore();
  });

  describe('updatePassword', () => {
    const fixtures = require('../fixtures/controllers/users/update-password');
    const { context, args, sendParams, response, logParams, error, errorHelperParams,
      argsWrongConfirmation, errorHelperParamsWrongConfirmation } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(gateway, 'sendUser').callsFake(() => Promise.resolve(response));
      sandbox.stub(errorHelper, 'handleResponse').callsFake(() => Promise.reject());
    });

    it('should return an error if the confirmation password is different', () => {

      return controller.updatePassword(null, argsWrongConfirmation, context)
        .should.be.rejected
        .then(() => {
          gateway.sendUser.called.should.be.false;
          log.message.called.should.be.false;
          errorHelper.handleResponse.calledOnce.should.be.true;
          errorHelper.handleResponse.args[0].should.be.eql(errorHelperParamsWrongConfirmation);
        });
    });

    it('should return an error if gateway.sendUser return a reject', () => {
      gateway.sendUser.restore();
      sandbox.stub(gateway, 'sendUser').callsFake(() => Promise.reject(error));

      return controller.updatePassword(null, args, context)
        .should.be.rejected
        .then(() => {
          gateway.sendUser.calledOnce.should.be.true;
          gateway.sendUser.args[0].should.eql(sendParams);
          log.message.called.should.be.false;
          errorHelper.handleResponse.calledOnce.should.be.true;
          errorHelper.handleResponse.args[0].should.be.eql(errorHelperParams);
        });
    });

    it('should return a success if there was no issue', () => {

      return controller.updatePassword(null, args, context)
        .should.be.fulfilled
        .then(result => {

          gateway.sendUser.calledOnce.should.be.true;
          gateway.sendUser.args[0].should.eql(sendParams);
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logParams);
          errorHelper.handleResponse.called.should.be.false;

          result.should.be.eql(response);
        });
    });
  });
});
