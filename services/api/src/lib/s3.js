const AWS = require('aws-sdk');
const config = require('../config/config');
const Logger = require('/var/lib/core/js/log');
const log = new Logger(module);

class S3 {
  constructor() {
    const options = {
      params: {
        Bucket: config.aws.s3.bucket
      },
      accessKeyId: config.aws.keyId,
      secretKey: config.aws.secretKey,
      region: config.aws.s3.region
    };

    this.s3 = new AWS.S3(options);
  }

  upload(key, body, guid = '') {
    const params = {
      Key: key,
      Body: body
    };

    return new Promise((resolve, reject) => {
      this.s3.putObject(params, (err) => {
        if (err) {
          log.error('File upload failed', guid, err);

          return reject(err);
        }

        log.message('File uploaded successfully', key, 'S3', guid);

        return resolve(key);
      });
    });
  }

  getUrl(key, guid) {

    return new Promise(resolve => {

      return this.s3.getSignedUrl('getObject', { Key: key }, (error, data) => {

        if (error) {

          log.error('Get file url', guid, error);

          return resolve('Url not found');
        }

        log.message('Get file url', key, 'S3', guid);

        return resolve(data);
      });
    });
  }

  delete(key, guid) {
    return new Promise((resolve) => {
      this.s3.deleteObject({ Key: key }, (err) => {
        if (err) {
          log.error('File delete failed', guid, err);

          return resolve(err);
        }

        log.message('File deleted successfully', key, 'S3', guid);

        return resolve(key);
      });
    });
  }
}

module.exports = new S3();
