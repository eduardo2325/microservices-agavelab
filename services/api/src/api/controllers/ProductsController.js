const gateway = require('../helpers/gateway');
const errorHelper = require('../helpers/error');
const log = new (require('/var/lib/core/js/log'))(module);

module.exports = {
  getProduct(root, args, context) {

    const request = {
      guid: context.guid,
      product_id: args.product_id
      // token: context.user.token
    };

    return gateway.sendUser('product', 'getProduct', request)
      .then(response => {

        log.message('Product', response, 'response', context.guid);

        return response;
      })
      .catch(error => errorHelper.handleResponse('product', error, context.guid));
  }
};
