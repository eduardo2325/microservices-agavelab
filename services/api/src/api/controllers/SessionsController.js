const gateway = require('../helpers/gateway');
const errorHelper = require('../helpers/error');
const log = new (require('/var/lib/core/js/log'))(module);

module.exports = {
  login(root, args, context) {
    const { input } = args;
    const request = {
      guid: context.guid,
      email: input.email,
      password: input.password
    };

    // console.log(request);

    return gateway.sendUser('session', 'create', request)
      .then(response => {

        log.message('Session login', response, 'response', context.guid);

        return response;
      })
      .catch(error => errorHelper.handleResponse('Session', error, context.guid));
  },

  logout(root, args, context) {
    const request = {
      guid: context.guid,
      token: context.user.token
    };

    return gateway.sendUser('session', 'deleteToken', request)
      .then(() => {
        const response = {
          success: true
        };

        log.message('Session logout', response, 'response', context.guid);

        return response;
      })
      .catch(error => errorHelper.handleResponse('Session', error, context.guid));
  },

  me(root, args, context) {
    const request = {
      guid: context.guid,
      token: context.user.token
    };
    
    return gateway.sendUser('session', 'me', request)
      .then(response => {

        log.message('Me', response, 'response', context.guid);

        return response;
      })
      .catch(error => errorHelper.handleResponse('Session', error, context.guid));
  }
};
