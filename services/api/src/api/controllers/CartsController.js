const gateway = require('../helpers/gateway');
const errorHelper = require('../helpers/error');
const log = new (require('/var/lib/core/js/log'))(module);

module.exports = {
  addToCart(root, args, context) {

    const request = {
      guid: context.guid,
      user_id: context.user.user_id,
      product_id: args.input.product_id,
      quantity: args.input.quantity
    };
    
    return gateway.sendUser('cart', 'addToCart', request)
      .then(response => {
        log.message('Add product to cart', response, 'response', context.guid);

        return response;
      })
      .catch(error => errorHelper.handleResponse('cart', error, context.guid));
  }
};
 