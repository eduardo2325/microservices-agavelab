const gateway = require('../helpers/gateway');
const errorHelper = require('../helpers/error');
const log = new (require('/var/lib/core/js/log'))(module);

module.exports = {
  updatePassword(root, args, context) {

    const { input } = args;
    const { old_password, new_password, confirmation_password } = input;
    const request = {
      guid: context.guid,
      user_id: context.user.user_id,
      old_password,
      new_password
    };

    if ( new_password !== confirmation_password) {
      const error = {
        path: 'confirmation_password',
        message: 'confirmation_password is different'
      };

      return errorHelper.handleResponse('User', error, context.guid);
    }

    return gateway.sendUser('user', 'updatePassword', request)
      .then(response => {

        log.message('Update password', response, 'response', context.guid);

        return response;
      })
      .catch(error => errorHelper.handleResponse('User', error, context.guid));
  }
};
