const proxy = new (require('../middleware/proxy'))('UsersController');

const resolvers = {
  Mutation: {
    updatePassword: proxy.authenticatedAndValidInput('updatePassword', 'user/update-password')
  }
};

module.exports = { resolvers };
