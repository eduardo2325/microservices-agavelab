// const productsController = require("../controllers/ProductsController");
const proxy = new (require('../middleware/proxy'))('ProductsController');

const dummyProduct = {
  idProduct: "1",
  code: "PANTS",
  name: "Pants",
  price: "5.00"
};

const resolvers = {
  Query: {
    // product: productsController.logDummy
    product: proxy.authenticated('getProduct')
  }
};

module.exports = { resolvers };
