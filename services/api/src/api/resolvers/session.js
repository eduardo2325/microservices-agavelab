const proxy = new (require('../middleware/proxy'))('SessionsController');

const resolvers = {
  Mutation: {
    login: proxy.validInput('login', 'session/login'),
    logout: proxy.authenticated('logout')
  },
  Query: {
    me: proxy.authenticated('me')
  }
};

module.exports = { resolvers };
