// const cartsController = require("../controllers/CartsController");
const proxy = new (require('../middleware/proxy'))('CartsController');

const resolvers = {
    Mutation: {
        // addToCart: cartsController.addToCart
        addToCart: proxy.authenticated('addToCart')
    }
    
};

module.exports = { resolvers };
