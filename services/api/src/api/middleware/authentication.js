const passport = require('passport');
const Strategy = require('passport-http-bearer').Strategy;
const gateway = require('../helpers/gateway');
const unauthorizedError = {
  path: 'user',
  message: 'Unauthorized',
  code: 101
};

passport.use(new Strategy(
  function(token, done) {
    /* istanbul ignore next */
    return gateway.sendUser('session', 'checkToken', { token })
      .then(user => {
        return Promise.resolve(done(null, user));
      })
      .catch(() => {
        return Promise.resolve(done(unauthorizedError));
      });
  }
));

module.exports = function(context) {
  const request = context;

  return new Promise((resolve, reject) => {

    return passport.authenticate('bearer', { session: false }, (err, user) => {
      if (err || !user) {
        const error = err || {};
        const originalError = error.path ? err : new Error(error.message);

        return reject(err ? originalError : unauthorizedError);
      }

      request.user = user;

      return resolve();
    })(request);
  });
};
