const authentication = require('../middleware/authentication');
const authorization = require('../middleware/authorization');
const { validate } = require('../middleware/validator');

class Proxy {
  constructor(controller) {
    this.controller = require(`../controllers/${controller}`);
  }

  authenticated(method) {

    return (root, args, context, info) => {

      return this._authenticated(context)
        .then(() => this.controller[method](root, args, context, info));
    };
  }

  authorized(method) {

    return (root, args, context, info) => {
      return this._authorized(context, info.fieldName)
        .then(() => this.controller[method](root, args, context, info));
    };
  }

  validInput(method, schemaName) {
    const schema = require(`../schemas/api/${schemaName}`);

    return (root, args, context, info) => {
      return this._validInput(context, schema, args.input)
        .then(() => this.controller[method](root, args, context, info));
    };
  }

  authenticatedAndValidInput(method, schemaName) {
    const schema = require(`../schemas/api/${schemaName}`);

    return (root, args, context, info) => {
      return this._authenticatedAndValidInput(context, method, schema, args.input)
        .then(() => this.controller[method](root, args, context, info));
    };
  }

  authorizedAndValidInput(method, schemaName) {
    const schema = require(`../schemas/api/${schemaName}`);

    return (root, args, context, info) => {
      return this._authorizedAndValidInput(context, info.fieldName, schema, args.input)
        .then(() => this.controller[method](root, args, context, info));
    };
  }

  _authenticated(context) {
    return authentication(context);
  }

  _authorized(context, method) {
    return authentication(context)
      .then(() => authorization.validate(method, context));
  }

  _authenticatedAndValidInput(context, method, schema, input) {
    return authentication(context)
      .then(() => validate(schema, context)(input));
  }

  _authorizedAndValidInput(context, method, schema, input) {
    return authentication(context)
      .then(() => authorization.validate(method, context))
      .then(() => validate(schema, context)(input));
  }

  _validInput(context, schema, input) {
    return validate(schema, context)(input);
  }
}

module.exports = Proxy;
