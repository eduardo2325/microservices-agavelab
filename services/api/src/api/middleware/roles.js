const userRoles = require('/var/lib/core/js/user-roles');

const roles  = {
};

if (process.env.NODE_ENV === 'test') {
  roles.me = [ 'TEST' ];
}

module.exports = roles;
