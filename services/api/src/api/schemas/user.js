const userTypes = require('./types/user');

const userDefs  = `
  extend type Mutation {
    updatePassword(input: UpdatePasswordInput!): User
  }
`;

module.exports = [ userTypes, userDefs ];
