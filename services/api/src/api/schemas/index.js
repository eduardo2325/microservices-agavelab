const { makeExecutableSchema } = require('graphql-tools');
const { resolvers } = require('../resolvers');
const userDefs = require('./user');
const sessionDefs = require('./session');
const productDefs = require('./product');
const cartDefs = require('./cart');

const SchemaDefinition  = `
type Query {
  dummy: [String]
}

type Mutation {
  dummy: [String]
}

type Subscription {
  dummy: [String]
}

type Success {
  success: Boolean
}

type PageInfo {
  pages: Int!
  total: Int!
  current_page: Int!
}

schema {
  query: Query
  mutation: Mutation
  subscription: Subscription
}
`;

const typeDefs = [
  SchemaDefinition,
  ...userDefs,
  ...sessionDefs,
  ...productDefs,
  ...cartDefs
];
const schema = makeExecutableSchema({ typeDefs, resolvers });

module.exports = schema;
