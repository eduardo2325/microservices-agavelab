const productTypes = require('./types/product');

const productDefs  = `
extend type Query {
    product(product_id: Int!): Product
  }
`;

module.exports = [ productTypes, productDefs ];
