const sessionTypes = require('./types/session');

const sessionDefs  = `
  extend type Mutation {
    login(input: LoginInput!): Me
    logout: Success
  }
  extend type Query {
    me: Me
  }
`;

module.exports = [ sessionTypes, sessionDefs ];
