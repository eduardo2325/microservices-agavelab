const updatePasswordSchema = {
  required: true,
  type: 'object',
  properties: {
    old_password: {
      type: 'string',
      required: true,
      minLength: 8
    },
    new_password: {
      type: 'string',
      required: true,
      minLength: 8
    },
    confirmation_password: {
      type: 'string',
      required: true,
      minLength: 8
    }
  }
};

module.exports = updatePasswordSchema;
