const cartTypes = require('./types/cart');

const cartDefs  = `
extend type Mutation {
    addToCart(input: ProductInput!): Summary
  }
`;

module.exports = [ cartTypes, cartDefs ];
