const SessionDefs = `
type Me {
  user_id: Int
  token: String
  role: String
  expiration_date: String
  email: String
}

input LoginInput {
  email: String!
  password: String!
}
`;

module.exports = SessionDefs;
