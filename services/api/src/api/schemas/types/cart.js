const CartDefs = `
input ProductInput {
  product_id: Int!
  quantity: Int!
}
type ProductInfo {
  name: String
  quantity: Int
  individualTotal: Float
}
type Summary {
    products: [ProductInfo]
    totalQuantity: Int
    totalPrice: Float
}
`;

module.exports = CartDefs;
