const UserDefs = `
  type User {
    id: Int
    first_name: String
    last_name: String
    email: String
    role: UserRole
    verified: Boolean
  }
  input UpdatePasswordInput {
    old_password: String!
    new_password: String!
    confirmation_password: String!
  }

  enum UserRole {
    ADMIN
  }
`;

module.exports = UserDefs;
