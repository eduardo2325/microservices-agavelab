const ProductDefs = `
type Product {
  product_id: Int
  code: String,
  name: String,
  price: Float
}
`;

module.exports = ProductDefs;
