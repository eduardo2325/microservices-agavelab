const fs = require('fs');
const _ = require('lodash');
const sizeOf = require('image-size');
const imageSize = require('../../config/config').image;
const s3 = require('../../lib/s3');

module.exports = {
  readFile: function(path) {

    return new Promise((resolve, reject) => {

      fs.readFile(path, (err, result) => {
        if (err) {
          return reject(err);
        }

        return resolve(result);
      });
    });
  },
  unlink: function(path) {

    return new Promise((resolve, reject) => {

      fs.unlink(path, (err, result) => {
        if (err) {
          return reject(err);
        }

        return resolve(result);
      });
    });
  },
  readDir: function(path) {

    return new Promise((resolve, reject) => {

      fs.readdir(path, (err, result) => {
        if (err) {
          return reject(err);
        }

        return resolve(result);
      });
    });
  },
  validateImage: function(context, userType, directory) {
    return Promise.resolve()
      .then(() => {
        if (!(context.files && context.files[0])) {
          return Promise.resolve();
        }

        if (!_.includes([ 'image/jpeg', 'image/png' ], context.files[0].mimetype)) {

          const error = {
            path: 'logo',
            message: 'Incorrect file type'
          };

          return Promise.reject(error);
        }

        const dimensions = sizeOf(context.files[0].path);

        if (dimensions.width > imageSize.width || dimensions.height > imageSize.height) {
          const error = {
            path: 'logo',
            message: 'Image size too big'
          };

          return Promise.reject(error);
        }

        return this.readFile(context.files[0].path);
      })
      .then(file => {
        if (file) {
          const originalname = context.files[0].originalname;
          const timestamp = Date.now();
          const filename = `${context.user.user_id}-${timestamp}.${originalname.split('.').pop()}`;
          const key = `${userType}/${context.user.user_id}/${directory}/${filename}`;

          return s3.upload(key, file, context.guid);
        }

        return Promise.resolve();
      });
  }
};
