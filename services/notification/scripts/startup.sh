#!/bin/bash

set -x -u

torus run -e "$TORUS_ENV" -s "$TORUS_SERVICE" --org agave-lab --project backend-base -- node app.js
