const BaseConsumer = require('/var/lib/core/js/kafka/base-consumer');
const Logger = require('/var/lib/core/js/log');
const log = new Logger(module);
const mailer = require('../../lib/mailer');

class UserConsumer extends BaseConsumer {
  constructor() {
    super('user');
  }

  passwordUpdated(event) {

    const { value } = event;
    const { body, guid } = value;

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      'password-updated',
      body,
      body.email,
      'Tu contraseña se ha modificado',
      guid
    ).catch(err => {
      log.error('Error sending updated password email', guid, err);
      throw err;
    });
  }
}

module.exports = new UserConsumer();
