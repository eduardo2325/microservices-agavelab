#!/bin/bash

set -x -u

torus run -e "$TORUS_ENV" -s "$TORUS_SERVICE" --org agave-lab --project backend-base -- ./node_modules/.bin/sequelize db:migrate --config config/config.js
torus run -e "$TORUS_ENV" -s "$TORUS_SERVICE" --org agave-lab --project backend-base -- ./node_modules/.bin/sequelize db:seed:all --config config/config.js --seeders-patj "$NODE_ENV"-seeders/
torus run -e "$TORUS_ENV" -s "$TORUS_SERVICE" --org agave-lab --project backend-base -- node app.js
