'use strict';
// This is a migration for the cart_items table
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('cart_items', {
            cart_items_id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            product_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'products',
                    key: 'product_id'
                }
            },
            quantity: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            cart_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'carts',
                    key: 'cart_id'
                }
            },
            created_at: {
                type:Sequelize.DATE
            },
            updated_at: {
                type:Sequelize.DATE
            },
            deleted_at: {
                type:Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('cart_items');
    }
};