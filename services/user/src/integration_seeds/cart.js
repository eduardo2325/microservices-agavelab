const { Cart } = require('../models');
const cartData = require('/var/lib/core/integration_fixtures/cart');

module.exports = {
    run: async () => {
        return await Cart.bulkCreate(cartData, {
            individualHooks: true
        });
    }
};
