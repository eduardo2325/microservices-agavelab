const { Product } = require('../models');
const productData = require('/var/lib/core/integration_fixtures/product');

module.exports = {
    run: async () => {
        return await Product.bulkCreate(productData, {
            individualHooks: true
        });
    }
};
