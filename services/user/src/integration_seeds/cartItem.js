const { CartItem } = require('../models');
const cartItemData = require('/var/lib/core/integration_fixtures/cartItem');

module.exports = {
    run: async () => {
        return await CartItem.bulkCreate(cartItemData, {
            individualHooks: true
        });
    }
};
