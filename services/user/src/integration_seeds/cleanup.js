const { sequelize } = require('../models');

module.exports = {
  run: async() => {
    await sequelize.query('DELETE FROM cart_items');
    await sequelize.query('DELETE FROM carts');
    await sequelize.query('DELETE FROM products');
    await sequelize.query('DELETE FROM sessions');
    await sequelize.query('DELETE FROM users');

  }
};
