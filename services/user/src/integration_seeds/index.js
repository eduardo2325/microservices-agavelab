const UserSeeds = require('./user');
const ProductSeeds = require('./product');
const CleanUpSeeds = require('./cleanup');
const CartSeeds = require('./cart');
const CartItemSeeds = require('./cartItem');

const { sequelize } = require('../models');

async function integrationSeeds() {
  try {
    await CleanUpSeeds.run();
    await UserSeeds.run();
    await ProductSeeds.run();
    // await CartSeeds.run();
    // await CartItemSeeds.run();
    return await sequelize.close();
  } catch (err) {
    console.log(err);

    return true;
  }
}

return integrationSeeds();
