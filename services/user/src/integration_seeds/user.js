const { User } = require('../models');
const userData = require('/var/lib/core/integration_fixtures/user');

module.exports = {
  run: async() => {
    return await User.bulkCreate(userData, { individualHooks: true });
  }
};
