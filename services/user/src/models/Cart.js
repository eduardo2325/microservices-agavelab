const Sequelize = require('sequelize');

const CartModel = {
    attributes: {
        cart_id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        user_id: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        created_at: {
            type:Sequelize.DATE
        },
        updated_at: {
            type:Sequelize.DATE
        },
        deleted_at: {
            type:Sequelize.DATE
        }
    },
    options: {
        tableName: 'carts',
        underscored: true,
        paranoid: true,
        classMethods: {
            associate: function(models) {
              this.hasMany(models.CartItem, {
                foreignKey: 'cart_id'
              });
            }
        }
    }
};

module.exports = CartModel;

