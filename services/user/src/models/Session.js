const Sequelize = require('sequelize');

const SessionModel = {
  attributes: {
    email: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: true
      }
    },
    token: {
      type: Sequelize.STRING
    },
    expiration_date: {
      type: Sequelize.DATE
    },
    role: {
      type: Sequelize.STRING,
      allowNull: false
    },
    user_id: {
      type: Sequelize.INTEGER,
      allowNull: false
    }
  },
  options: {
    tableName: 'sessions',
    underscored: true,
    paranoid: true,
    classMethods: {
      associate: function(models) {
        this.belongsTo(models.User);
      }
    }
  }
};

module.exports = SessionModel;
