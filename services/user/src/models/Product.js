const Sequelize = require('sequelize');

const ProductModel = {
    attributes: {
        product_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
        },
        code: {
            type: Sequelize.STRING,
            validate: {
                notEmpty: true
            }
        },
        name: {
            type: Sequelize.STRING,
            validate: {
                notEmpty: true
            }
        },
        price: {
            type: Sequelize.FLOAT,
            allowNull: false
        },
        deleted_at: {
            type: Sequelize.DATE
        }
    },
    options: {
        tableName: 'products',
        underscored: true,
        paranoid: true
    }
};

module.exports = ProductModel;
