const Sequelize = require('sequelize');

const CartItemModel = {
    attributes: {
        cart_items_id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        product_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        quantity: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        cart_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        created_at: {
            type:Sequelize.DATE
        },
        updated_at: {
            type:Sequelize.DATE
        },
        deleted_at: {
            type:Sequelize.DATE
        }
    },
    options: {
        tableName: 'cart_items',
        underscored: true,
        paranoid: true,
        classMethods: {
            associate: function(models) {
                this.belongsTo(models.Cart, {
                    targetKey: 'cart_id',
                    foreignKey: 'cart_id'
                  }
                );            
            }
        }
    }
};

module.exports = CartItemModel;
