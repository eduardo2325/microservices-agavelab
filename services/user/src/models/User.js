const Sequelize = require('sequelize');
const userRoles = require('/var/lib/core/js/user-roles');

const UserModel = {
  attributes: {
    first_name: {
      type: Sequelize.STRING,
      validate: {
        notEmpty: true
      }
    },
    last_name: {
      type: Sequelize.STRING,
      validate: {
        notEmpty: true
      }
    },
    email: {
      type: Sequelize.STRING,
      validate: {
        isEmail: true
      }
    },
    role: {
      type: Sequelize.STRING,
      validate: {
        isIn: [ Object.values(userRoles) ]
      }
    },
    password: {
      type: Sequelize.STRING
    },
    verified: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },
    deleted_at: {
      type: Sequelize.DATE
    }
  },
  options: {
    tableName: 'users',
    underscored: true,
    paranoid: true
  }
};

module.exports = UserModel;
