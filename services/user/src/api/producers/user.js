const BaseProducer = require('/var/lib/core/js/kafka/base-producer');
const log = new (require('/var/lib/core/js/log'))(module);

class UserProducer extends BaseProducer {
  constructor() {
    super('user');
  }

  passwordUpdated(user, guid) {

    const event = {
      message: {
        type: 'PasswordUpdated',
        body: {
          username: user.first_name,
          email: user.email
        },
        guid
      },
      key: user.id
    };

    log.message('Producing user event', event, 'Event', guid);

    return this.produce(event);
  }
}

module.exports = new UserProducer();
