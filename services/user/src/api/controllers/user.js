const log = new (require('/var/lib/core/js/log'))(module);
const errorHandler = require('../helpers/error');
const { User } = require('../../models');
const { userProducer } = require('../producers');

class UserController {
  updatePassword({ request }, callback) {

    const { user_id, old_password, new_password } = request;
    let userInstance;

    log.message('Update password', request, 'request', request.guid);

    return User.updatePassword(user_id, old_password, new_password)
      .then(user => {

        userInstance = user;

        return userProducer.passwordUpdated(userInstance, request.guid);
      }).then(() => {
        const response = userInstance.getRaw();

        log.message('Update password', response, 'response', request.guid);

        callback(null, response);
      })
      .catch(err => errorHandler.format(err, request.guid, callback));
  }
}

module.exports = new UserController();
