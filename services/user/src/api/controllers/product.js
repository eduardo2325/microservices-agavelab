const log = new(require('/var/lib/core/js/log'))(module);
const errorHandler = require('../helpers/error');
const _ = require('lodash');
const { Product } = require('../../models');


class ProductController {

    getProduct({request}, callback) {

        log.message('Get a product information', request, 'request', request.guid);
        const query = {
            where: {
                product_id: request.product_id
            }
        }

        return Product.findOne(query)
        .then(product => {
            if (!product) {
            const error = {
                errors: [ {
                path: 'product',
                message: 'Not found'
                } ]
            };

            return Promise.reject(error);
            }

            return product.getRaw();
        }).then(product => {
            log.message('Get a product information', product, 'response', request.guid);

            return callback(null, product);
        }).catch(err => {
            return errorHandler.format(err, request.guid, callback);
        });
       
    }
}

module.exports = new ProductController();
