const log = new (require('/var/lib/core/js/log'))(module);
const errorHandler = require('../helpers/error');
const _ = require('lodash');
const { Session } = require('../../models');

class SessionController {

  create({ request }, callback) {

    log.message('Session create', request, 'request', request.guid);

    return Session.verifyAndCreate(request.email, request.password)
      .then(session => {

        const response = {
          token: session.session.token,
          expiration_date: session.session.expiration_date.toISOString(),
          user_id: session.user.id,
          email: session.user.email,
          role: session.user.role
        };

        log.message('Session create', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => errorHandler.format(err, request.guid, callback));
  }

  deleteToken({ request }, callback) {
    log.message('Delete token', request, 'request', request.guid);

    const query = {
      where: {
        token: request.token
      }
    };

    return Session.destroy(query)
      .then(() => {
        log.message('Delete token', {}, 'response', request.guid);

        return callback(null, {});
      })
      .catch(err => errorHandler.format(err, request.guid, callback));
  }

  checkToken({ request }, callback) {
    log.message('Check token', request, 'request', request.guid);
    return Session.checkToken(request.token)
      .then(session => {
        const response = _.omit(session.get(), [ 'id', 'created_at', 'updated_at', 'deleted_at' ]);

        response.expiration_date = response.expiration_date.toISOString();

        log.message('Check token', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => errorHandler.format(err, request.guid, callback));
  }

  me({ request }, callback) {
    log.message('Me', request, 'request', request.guid);
    return Session.findOne({
      where: {
        token: request.token
      }
    })
      .then(session => {
        const response = _.omit(session.get(), [ 'id', 'created_at', 'updated_at', 'deleted_at' ]);

        response.expiration_date = response.expiration_date.toISOString();

        log.message('Me', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => errorHandler.format(err, request.guid, callback));
  }
}

module.exports = new SessionController();
