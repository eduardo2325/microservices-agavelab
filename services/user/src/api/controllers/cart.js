const log = new(require('/var/lib/core/js/log'))(module);
const errorHandler = require('../helpers/error');
const _ = require('lodash');
const { Product, Cart, CartItem} = require('../../models');

class CartController {

    addToCart({request}, callback) {

        log.message('Add product to cart', request, 'request', request.guid);

        return Cart.createOrUpdateCart(request.user_id, request.product_id, request.quantity)
        .then(response => {
            log.message('Cart summary', response, 'response', request.guid);
            return callback(null, response);
        }).catch(err => errorHandler.format(err, request.guid, callback));

    }
}

module.exports = new CartController();
