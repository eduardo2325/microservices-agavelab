class  CartHelper {
  checkout(products) {
    let totalQuantity = 0;
    let totalPrice = 0;
    let aux = 0;
    let productsList = Array();
    let summary = {};

    products.forEach((value, index) => {
        var product = {};

        product.name = value.name;
        product.quantity = value.quantity;
        
        if(value.code == 'TSHIRT' && value.quantity >= 3){
          aux = (19.00 * value.quantity);
        } else {
          aux = (value.price * value.quantity);
        }

        totalPrice = totalPrice + aux;
        totalQuantity = totalQuantity + value.quantity;
        product.individualTotal = aux;
        product.individualTotal = Number(parseFloat(product.individualTotal).toFixed(2));
        productsList.push(product);
    });
    totalQuantity = totalQuantity;
    totalPrice = Number(parseFloat(totalPrice).toFixed(2));
    
    summary.products = productsList;
    summary.totalQuantity = totalQuantity;
    summary.totalPrice = totalPrice;

    return summary;
  }
}

module.exports = new CartHelper();
