const bcrypt = require('bcryptjs');
const saltRounds = 10;

class CryptoHelper {
  encrypt(value) {
    const salt = bcrypt.genSaltSync(saltRounds);
    const hash = bcrypt.hashSync(value, salt);

    return hash;
  }
}

module.exports = new CryptoHelper();
