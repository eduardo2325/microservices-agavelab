const CartHelper = require("../helpers/cart");

module.exports = {
    options: {
        classMethods: {
            createOrUpdateCart: function(user_id, product_id, quantity) {
                const { Cart, CartItem, Product } = require('../../models');
                const cartData = {
                    user_id: user_id
                }

                return Cart.findOne({
                    where: {user_id: user_id}
                }).then(cart => {
                    if(cart){
                        cartInfo = {
                            cart_id: cart.cart_id,
                            user_id: cart.user_id
                        }
                        return cartInfo;
                    } else {
                        return Cart.create(cartData).then(cart => {
                            if(!cart){
                                const error = {
                                    errors: [{
                                        path: 'cart',
                                        message: 'Invalid user_id'
                                    }]
                                };
                                return Promise.reject(error);
                            }
                            cartInfo = {
                                cart_id: cart.cart_id,
                                user_id: cart.user_id
                            }
                            return cartInfo;
                        })
                        
                    }
                }).then(cart => {
                    return CartItem.createOrUpdateCartItem(product_id, cart.cart_id, quantity);
                })
                /*.then(cartItems => {
                    var products = Array();

                    let promises = cartItems.map(currentItem => {
                        const query = {
                            where: {
                                product_id: currentItem.dataValues.product_id,
                            }
                        };
 
                        return Product.findOne(query).then(product => {
                         
                            let auxProduct = product.getRaw();
                            auxProduct.quantity = currentItem.dataValues.quantity;
                            return auxProduct;
                        })
                    });

                    return Promise.all(promises).then(function(results){
                        return CartHelper.checkout(results);
                    })
                }).then(results => {
                    return results;
                }) */
            }
        },
        instanceMethods: {
            getInfo: function () {
                const response = {
                    cart_id: this.cart_id,
                    user_id: this.user_id
                };

                return response;
            }
        }
    }
};
