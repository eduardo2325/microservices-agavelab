module.exports = {
    options: {
        instanceMethods: {
            getRaw: function () {
                const response = {
                    product_id: this.product_id,
                    code: this.code,
                    name: this.name,
                    price: this.price,
                };

                return response;
            }
        }
    }
};
