module.exports = {
    options: {
        classMethods: {
            createOrUpdateCartItem: function(product_id, cart_id, quantity){
                const { CartItem } = require('../../models');
                const oneProduct = {
                    where: {
                        product_id: product_id,
                        cart_id: cart_id
                    }
                };

                return CartItem.findOne(oneProduct)
                    .then(cartItem => { 
                        return cartItem
                }).then(cartItem => {
                    const cartItemData = {
                        product_id: product_id,
                        quantity: quantity,
                        cart_id: cart_id,
                    }
                    const allProducts = {
                        where: {
                            cart_id: cart_id
                        }
                    };

                    if (!cartItem) {
                        return CartItem.create(cartItemData).then(product => {
                            return CartItem.findAll(allProducts).then(products => {
                                return products;
                            });
                        });

                    } else {
                        let newQuantity = 0;

                        newQuantity = cartItem.quantity + quantity;
                        return CartItem.update({quantity:newQuantity}, oneProduct).then(updatedCart => {
                            
                            return CartItem.findAll(allProducts).then(products => {
                                return products;
                            });
                        });
                        // return CartItem.findById(cart_id);
                    }
                    
                })  
            }
        },
        instanceMethods: {
            getInfo: function () {
                const response = {
                    cart_items_id: this.cart_items_id,
                    product_id: this.product_id,
                    quantity: this.quantity,
                    cart_id: this.cart_id
                };

                return response;
            }
        }
    }
};
