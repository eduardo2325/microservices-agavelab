const utils = require('../helpers/utils');
const randtoken = require('rand-token');

module.exports = {
  options: {
    hooks: {
      beforeCreate: function(instance) {

        instance.expiration_date = instance.defineExpiration(instance.role);
        instance.token = randtoken.generate(16);

        return instance;
      }
    },
    classMethods: {
      verifyAndCreate: function(email, password) {

        const { User, Session } = require('../../models');
        const response = {};

        return User.verify(email, password)
          .then(user => {
            if (!user.verified) {
              return Promise.reject({
                errors: [ {
                  path: 'user',
                  message: 'Not verified'
                } ]
              });
            }
            return user;
          }).then(user => {
            const sessionData = {
              user_id: user.id,
              email: user.email,
              role: user.role
            };

            response.user = user;

            return Session.create(sessionData);
          }).then(session => {

            response.session = session;

            return Promise.resolve(response);
          }).catch(err => {
            const error = {
              errors: [ {
                path: 'email or password',
                message: 'email or password doesn\'t match'
              } ]
            };

            return Promise.reject(err || error);
          });
      },
      checkToken: function(token) {
        return this.findOne({
          where: {
            token
          }
        })
          .then(session => {
            if (!session || new Date() >= session.expiration_date) {
              return Promise.reject({
                errors: [ {
                  path: 'token',
                  message: 'Token expired'
                } ]
              });
            }

            session.expiration_date = session.defineExpiration(session.role);
            return session.save();
          });
      }
    },
    instanceMethods: {
      defineExpiration: function(role) {
        const time = {
          ADMIN: 45
        };

        return utils.expirationTime(time[role]);
      },
      getUserInfo: function(){
        const response = {
          user_id: this.user_id,
          email: this.email
        };

        return response;
      }
    }
  }
};
