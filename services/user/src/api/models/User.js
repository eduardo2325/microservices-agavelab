const bcrypt = require('bcryptjs');
const Crypto = require('../helpers/crypto');

module.exports = {
  options: {
    hooks: {
      beforeCreate: function(instance) {
        const hash = Crypto.encrypt(instance.password);

        instance.password = hash;
        return instance;
      }
    },
    classMethods: {
      verify: function(email, password, userId) {
        const { User } = require('../../models');
        const query = {
          where: {
            email
          }
        };
        let user = null;

        if (!email && userId) {
          query.where = {
            id: userId
          };
        }

        return User.findOne(query)
          .then(u => {
            user = u;
            if (!user) {
              return Promise.reject();
            }

            return bcrypt.compare(password, user.password);
          }).then(result => {
            if (!result) {
              return Promise.reject();
            }

            return user;
          });
      },
      updatePassword: function(userId, oldPassword, newPassword) {
        const { User } = require('../../models');

        return User.verify(null, oldPassword, userId).then(user => {

          const encrypted = Crypto.encrypt(newPassword);

          user.password = encrypted;

          return user.save();
        }).catch(err => {
          const error = {
            errors: [ {
              path: 'old_password',
              message: 'old_password is incorrect'
            } ]
          };

          return Promise.reject(err || error);
        });
      }
    },
    instanceMethods: {
      getRaw: function() {
        const response = {
          id: this.id,
          first_name: this.first_name,
          last_name: this.last_name,
          email: this.email,
          role: this.role || '',
          verified: this.verified
        };

        return response;
      }
    }
  }
};
