function waitForAppStart(server, done) {
  if (server.server.started) {
    return done();
  }

  return setTimeout(() => {
    waitForAppStart(server, done);
  }, 1000);
}

before(function(done) {
  waitForAppStart(require('../app'), done);
});

after(function(done) {
  const server = require('../app');

  server.shutdown();
  done();
});
