const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();

const SessionModel = require('../../../api/models/Session');
const optionsModel = SessionModel.options;

const randtoken = require('rand-token');
const { User, Session } = require('../../../models');
const utils = require('../../../api/helpers/utils');


chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Session controller', () => {

  afterEach(() => {
    sandbox.restore();
  });

  describe('hooks', () => {

    describe('beforeCreate', () => {
      const { hooks } = optionsModel;
      const fixtures = require('./fixtures/session/before-create');
      const { instance, token, date, response } = fixtures;

      beforeEach(() => {
        sandbox.stub(randtoken, 'generate').callsFake(() => token);
        sandbox.stub(instance, 'defineExpiration').callsFake(() => date);
      });

      it('should return a success if there was no issue', () => {

        const data = hooks.beforeCreate(instance);

        data.expiration_date.should.be.eql(response.expiration_date);
        data.token.should.be.eql(response.token);
      });
    });
  });

  describe('classMethods', () => {

    describe('verifyAndCreate', () => {
      const { classMethods } = optionsModel;
      const fixtures = require('./fixtures/session/verify-and-create');
      const { email, password, verifyParams, userInstance, userNotVerified, createParams,
        sessionInstance, response, commonError, notVerifiedError, wrongDataError } = fixtures;

      beforeEach(() => {
        sandbox.stub(User, 'verify').callsFake(() => Promise.resolve(userInstance));
        sandbox.stub(Session, 'create').callsFake(() => Promise.resolve(sessionInstance));
      });

      it('should return an error if there was an issue with User.verify', () => {
        User.verify.restore();
        sandbox.stub(User, 'verify').callsFake(() => Promise.reject());

        return classMethods.verifyAndCreate(email, password)
          .should.be.rejected
          .then(error => {

            User.verify.calledOnce.should.be.true;
            User.verify.args[0].should.be.eql(verifyParams);
            Session.create.called.should.be.false;

            error.should.be.eql(wrongDataError);
          });
      });

      it('should return an error if User.verify returns a not verified user', () => {
        User.verify.restore();
        sandbox.stub(User, 'verify').callsFake(() => Promise.resolve(userNotVerified));

        return classMethods.verifyAndCreate(email, password)
          .should.be.rejected
          .then(error => {

            User.verify.calledOnce.should.be.true;
            User.verify.args[0].should.be.eql(verifyParams);
            Session.create.called.should.be.false;

            error.should.be.eql(notVerifiedError);
          });
      });

      it('should return an error if there was an issue with Session.create', () => {
        Session.create.restore();
        sandbox.stub(Session, 'create').callsFake(() => Promise.reject(commonError));

        return classMethods.verifyAndCreate(email, password)
          .should.be.rejected
          .then(error => {

            User.verify.calledOnce.should.be.true;
            User.verify.args[0].should.be.eql(verifyParams);
            Session.create.calledOnce.should.be.true;
            Session.create.args[0].should.be.eql(createParams);

            error.should.be.eql(commonError);
          });
      });

      it('should return a success if there was no issue', () => {

        return classMethods.verifyAndCreate(email, password)
          .should.be.fulfilled
          .then(result => {

            User.verify.calledOnce.should.be.true;
            User.verify.args[0].should.be.eql(verifyParams);
            Session.create.calledOnce.should.be.true;
            Session.create.args[0].should.be.eql(createParams);

            result.should.be.eql(response);
          });
      });
    });

    describe('checkToken', () => {

      const { classMethods } = optionsModel;
      const fixtures = require('./fixtures/session/check-token');
      const { context, token, findOneParams, validSession, invalidSession,
        commonError, expiredError } = fixtures;

      beforeEach(() => {
        sandbox.stub(context, 'findOne').callsFake(() => Promise.resolve(validSession));
        sandbox.stub(validSession, 'save').callsFake(() => Promise.resolve());
        sandbox.stub(validSession, 'defineExpiration').callsFake(() => 10);
      });

      it('should return error if findOne return an error', () => {
        context.findOne.restore();
        sandbox.stub(context, 'findOne').callsFake(() => Promise.reject(commonError));

        return classMethods.checkToken.call(context, token)
          .should.be.rejected
          .then(error => {
            context.findOne.calledOnce.should.be.true;
            context.findOne.args[0].should.be.eql(findOneParams);
            validSession.save.called.should.be.false;

            error.should.be.eql(commonError);
          });
      });

      it('should return error if session does not exists', () => {
        context.findOne.restore();
        sandbox.stub(context, 'findOne').callsFake(() => Promise.resolve());

        return classMethods.checkToken.call(context, token)
          .should.be.rejected
          .then(error => {
            context.findOne.calledOnce.should.be.true;
            context.findOne.args[0].should.be.eql(findOneParams);
            validSession.save.called.should.be.false;

            error.should.be.eql(expiredError);
          });
      });

      it('should return error if session was expired', () => {
        context.findOne.restore();
        sandbox.stub(context, 'findOne').callsFake(() => Promise.resolve(invalidSession));

        return classMethods.checkToken.call(context, token)
          .should.be.rejected
          .then(error => {
            context.findOne.calledOnce.should.be.true;
            context.findOne.args[0].should.be.eql(findOneParams);
            validSession.save.called.should.be.false;

            error.should.be.eql(expiredError);
          });
      });

      it('should return a successful response', () => {
        return classMethods.checkToken.call(context, token)
          .should.be.fulfilled
          .then(() => {
            context.findOne.calledOnce.should.be.true;
            context.findOne.args[0].should.be.eql(findOneParams);
            validSession.save.calledOnce.should.be.true;
          });
      });
    });
  });

  describe('instanceMethods', () => {

    describe('defineExpiration', () => {

      const { instanceMethods } = optionsModel;
      const fixtures = require('./fixtures/session/define-expiration');
      const { admin, professionist, educational, colaborator, distributor } = fixtures;

      beforeEach(() => {
        sandbox.stub(utils, 'expirationTime').callsFake((time) => time);
      });

      it('should return 45 minutes if the user role is ADMIN', () => {
        const expiration = instanceMethods.defineExpiration(admin.role);

        expiration.should.be.eql(admin.time);
      });
    });
  });
});
