const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();

const CartModel = require('../../../api/models/Cart');
const { Cart, CartItem, sequelize } = require('../../../models');
const optionsModel = CartModel.options;

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Cart model', () => {

  afterEach(() => {
    sandbox.restore();
  });

  describe('classMethods', () => {
    describe('createOrUpdateCart', () => {

      const { classMethods } = optionsModel;
      const fixtures = require('./fixtures/cart/add-to-cart');
      const { commonError, cart, user_id, product_id, quantity, findOneParams, cartItems } = fixtures;

      beforeEach(() => {
        sandbox.stub(Cart, 'findOne').resolves(cart);
        sandbox.stub(CartItem, 'createOrUpdateCartItem').resolves(cartItems);
        // sandbox.stub(Cart.createOrUpdateCart, 'spread').resolves(cart);
        // sandbox.stub(bcrypt, 'compare').callsFake(() => Promise.resolve(true));
      });

      it.only('should return a succes if there was no error', () => {

        return classMethods.createOrUpdateCart(user_id, product_id, quantity)
          .should.be.fulfilled
          .then(response => {
              Cart.findOne.calledOnce.should.be.true;
              Cart.findOne.args[0].should.be.eql(findOneParams);
              CartItem.createOrUpdateCartItem.calledOnce.should.be.true;
            // console.log(Cart.findOne.args[0]);
            // console.log(findOneParams);
            // console.log(response);
            // User.verify.calledOnce.should.be.true;
            // User.verify.args[0].should.be.eql(verifyParams);
            // crypto.encrypt.calledOnce.should.be.true;
            // crypto.encrypt.args[0].should.be.eql([ newPassword ]);
            // user.save.calledOnce.should.be.true;
          });
      });

      
    });
   
  });

});
