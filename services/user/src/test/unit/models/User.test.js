const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();

const bcrypt = require('bcryptjs');
const UserModel = require('../../../api/models/User');
const { User } = require('../../../models');
const crypto = require('../../../api/helpers/crypto');
const optionsModel = UserModel.options;

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/User model', () => {

  afterEach(() => {
    sandbox.restore();
  });

  describe('hooks', () => {
    describe('beforeCreate', () => {

      const { hooks } = optionsModel;
      const fixtures = require('./fixtures/user/before-create');
      const { instance, salt, genSaltParams, hash, hashSyncParams } = fixtures;

      beforeEach(() => {
        sandbox.stub(bcrypt, 'genSaltSync').callsFake(() => salt);
        sandbox.stub(bcrypt, 'hashSync').callsFake(() => hash);
      });

      it('should return a successful response', () => {
        const oldPassword = instance.password;
        const result = hooks.beforeCreate(instance);

        result.password.should.be.not.equal(oldPassword);
        bcrypt.genSaltSync.calledOnce.should.be.true;
        bcrypt.genSaltSync.args[0].should.be.eql(genSaltParams);
        bcrypt.hashSync.calledOnce.should.be.true;
        return bcrypt.hashSync.args[0].should.be.eql(hashSyncParams);
      });
    });
  });

  describe('classMethods', () => {
    describe('verify', () => {

      const { classMethods } = optionsModel;
      const fixtures = require('./fixtures/user/verify');
      const { user, email, password, compareParams, userQuery, commonError } = fixtures;

      beforeEach(() => {
        sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(user));
        sandbox.stub(bcrypt, 'compare').callsFake(() => Promise.resolve(true));
      });

      it('should return error if User.findOne has an error', () => {
        User.findOne.restore();
        sandbox.stub(User, 'findOne').callsFake(() => Promise.reject(commonError));

        return classMethods.verify(email, password)
          .should.be.rejected
          .then(() => {
            User.findOne.calledOnce.should.be.true;
            User.findOne.args[0].should.be.eql(userQuery);
            bcrypt.compare.called.should.be.false;
          });
      });

      it('should return error response if the user does not exists', () => {
        User.findOne.restore();
        sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve());

        return classMethods.verify(email, password)
          .should.be.rejected
          .then(() => {
            User.findOne.calledOnce.should.be.true;
            User.findOne.args[0].should.be.eql(userQuery);
            bcrypt.compare.called.should.be.false;
          });
      });

      it('should return error if bcrypt has an error', () => {
        bcrypt.compare.restore();
        sandbox.stub(bcrypt, 'compare').callsFake(() => Promise.reject(commonError));

        return classMethods.verify(email, password)
          .should.be.rejected
          .then(() => {
            User.findOne.calledOnce.should.be.true;
            User.findOne.args[0].should.be.eql(userQuery);
            bcrypt.compare.calledOnce.should.be.true;
            bcrypt.compare.args[0].should.be.eql(compareParams);
          });
      });

      it('should return error if the password is no t the same with the one stored in the db', () => {

        bcrypt.compare.restore();
        sandbox.stub(bcrypt, 'compare').callsFake(() => Promise.resolve(false));

        return classMethods.verify(email, password)
          .should.be.rejected
          .then(() => {
            User.findOne.calledOnce.should.be.true;
            User.findOne.args[0].should.be.eql(userQuery);
            bcrypt.compare.calledOnce.should.be.true;
            bcrypt.compare.args[0].should.be.eql(compareParams);
          });
      });

      it('should return user if ther was no issue verifying it', () => {

        return classMethods.verify(email, password)
          .should.be.fulfilled
          .then(() => {
            User.findOne.calledOnce.should.be.true;
            User.findOne.args[0].should.be.eql(userQuery);
            bcrypt.compare.calledOnce.should.be.true;
            bcrypt.compare.args[0].should.be.eql(compareParams);
          });
      });
    });

    describe('updatePassword', () => {
      const { classMethods } = optionsModel;
      const fixtures = require('./fixtures/user/update-password');
      const { userId, oldPassword, newPassword, encrypted, verifyParams,
        user, commonError, wrongPasswordError } = fixtures;

      beforeEach(() => {
        sandbox.stub(User, 'verify').callsFake(() => Promise.resolve(user));
        sandbox.stub(crypto, 'encrypt').callsFake(() => encrypted);
        sandbox.stub(user, 'save').callsFake(() => Promise.resolve(user));
        sandbox.stub(user, 'getInfo').callsFake(() => Promise.resolve(user));
      });

      it('should return error if User.verify has an error', () => {
        User.verify.restore();
        sandbox.stub(User, 'verify').callsFake(() => Promise.reject(commonError));

        return classMethods.updatePassword(userId, oldPassword, newPassword)
          .should.be.rejected
          .then(error => {

            User.verify.calledOnce.should.be.true;
            User.verify.args[0].should.be.eql(verifyParams);
            crypto.encrypt.called.should.be.false;
            user.save.called.should.be.false;
            user.getInfo.called.should.be.false;

            error.should.be.eql(commonError);
          });
      });

      it('should return error if User.verify does not match a user with the info provided', () => {
        User.verify.restore();
        sandbox.stub(User, 'verify').callsFake(() => Promise.reject());

        return classMethods.updatePassword(userId, oldPassword, newPassword)
          .should.be.rejected
          .then(error => {

            User.verify.calledOnce.should.be.true;
            User.verify.args[0].should.be.eql(verifyParams);
            crypto.encrypt.called.should.be.false;
            user.save.called.should.be.false;
            user.getInfo.called.should.be.false;

            error.should.be.eql(wrongPasswordError);
          });
      });

      it('should return error if user.save hasn an error', () => {
        user.save.restore();
        sandbox.stub(user, 'save').callsFake(() => Promise.reject(commonError));

        return classMethods.updatePassword(userId, oldPassword, newPassword)
          .should.be.rejected
          .then(error => {

            User.verify.calledOnce.should.be.true;
            User.verify.args[0].should.be.eql(verifyParams);
            crypto.encrypt.calledOnce.should.be.true;
            crypto.encrypt.args[0].should.be.eql([ newPassword ]);
            user.save.calledOnce.should.be.true;
            user.getInfo.called.should.be.false;

            error.should.be.eql(commonError);
          });
      });

      it('should return a succes if there was no error', () => {

        return classMethods.updatePassword(userId, oldPassword, newPassword)
          .should.be.fulfilled
          .then(() => {

            User.verify.calledOnce.should.be.true;
            User.verify.args[0].should.be.eql(verifyParams);
            crypto.encrypt.calledOnce.should.be.true;
            crypto.encrypt.args[0].should.be.eql([ newPassword ]);
            user.save.calledOnce.should.be.true;
          });
      });
    });
  });

  describe('instanceMethods', () => {
    describe('getRaw', () => {

      const { instanceMethods } = optionsModel;
      const fixtures = require('./fixtures/user/get-raw');
      const { user, userWithoutRole, response, responseWithoutRole } = fixtures;

      it('should return a succes with role', () => {

        const result = instanceMethods.getRaw.call(user);

        result.should.be.eql(response);
      });

      it('should return a succes without role', () => {

        const result = instanceMethods.getRaw.call(userWithoutRole);

        result.should.be.eql(responseWithoutRole);
      });
    });
  });
});
