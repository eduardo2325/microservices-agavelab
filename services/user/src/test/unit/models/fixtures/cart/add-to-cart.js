const { request, errors } = require('../../../../common/fixtures');
const { guid, requestType, responseType } = request;
const { commonError } = errors;
const _ = require('lodash');

/* const FindOrCreateResponse = {
    [ Cart {
        dataValues:
         { cart_id: 1,
           user_id: 1,
           created_at: '2018-06-14T21:32:47.540Z',
           updated_at: '2018-06-14T21:32:47.540Z',
           deleted_at: null },
        _previousDataValues:
         { cart_id: 1,
           user_id: 1,
           created_at: '2018-06-14T21:32:47.540Z,'
           updated_at: '2018-06-14T21:32:47.540Z',
           deleted_at: null },
        _changed: {},
        _modelOptions:
         { timestamps: true,
           validate: {},
           freezeTableName: false,
           underscored: true,
           underscoredAll: false,
           paranoid: true,
           rejectOnEmpty: false,
           whereCollection: [Object],
           schema: null,
           schemaDelimiter: '',
           defaultScope: {},
           scopes: [],
           indexes: [],
           name: [Object],
           omitNull: false,
           tableName: 'carts',
           classMethods: [Object],
           instanceMethods: [Object],
           sequelize: [Object],
           hooks: {},
           uniqueKeys: {} },
        _options:
         { isNewRecord: false,
           _schema: null,
           _schemaDelimiter: '',
           raw: true,
           attributes: [Array] },
        __eagerlyLoadedAssociations: [],
        isNewRecord: false },
      false ]
} */
const grpcRequest = {
    guid,
    user_id: 1,
    product_id: 1,
    quantity: 2
};
const cart = {
    cart_id: 1,
    user_id: 1,
    getInfo: () => {
        return;
    }
};
const cartItem = {
    cart_items_id: 1,
    product_id: 1,
    quantity: 2,
    cart_id: 1,
    // created_at: new Date(),
    // updated_at: new Date()
};
const summary = {
    products: [
        {
            name: 'Pants',
            quantity: 2,
            individualTotal: 10
        }
    ],
    totalQuantity: 2,
    totalPrice: 10
};
const cartItems = {
    cartItems: [cartItem]
}
const mainRequest = _.cloneDeep(grpcRequest)
const findOneParams = [{
    where: {
        user_id: 1
    }
}];

module.exports = {
    user_id: 1,
  product_id: 1,
  quantity: 1,
  guid,
  mainRequest,
  findOneParams,
  cart,
  summary,
  cartItems,
  logRequestParams: [
    'Add product to cart',
    grpcRequest,
    requestType,
    guid
  ],
//   createOrUpdateCartParams: createOrUpdateCartParams,
  error: commonError
};
