const { request, errors } = require('../../../../common/fixtures');
const { token } = request;
const { commonError } = errors;

const date = new Date();

module.exports = {
  context: {
    findOne: () => {
      return;
    }
  },
  findOneParams: [ {
    where: {
      token
    }
  } ],
  token,
  validSession: {
    expiration_date: date.setMinutes(date.getMinutes() + 30),
    defineExpiration: () => {
      return;
    },
    save: () => {
      return;
    }
  },
  invalidSession: {
    expiration_date: date.setMinutes(date.getMinutes() - 30),
    save: () => {
      return;
    }
  },
  commonError,
  expiredError: {
    errors: [ {
      path: 'token',
      message: 'Token expired'
    } ]
  }
};
