const { request } = require('../../../../common/fixtures');
const { token } = request;

const date = new Date();
const instance = {
  role: 'ADMIN',
  defineExpiration: () => {
    return;
  }
};

module.exports = {
  instance,
  token,
  date,
  response: {
    role: 'ADMIN',
    expiration_date: date,
    token,
    defineExpiration: () => {
      return;
    }
  }
};
