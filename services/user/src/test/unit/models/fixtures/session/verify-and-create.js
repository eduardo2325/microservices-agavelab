const { request, errors } = require('../../../../common/fixtures');
const { token } = request;
const { commonError } = errors;

const email = 'some@email.com';
const password = 'some password';
const userInstance = {
  id: 1,
  role: 'ADMIN',
  email,
  verified: true
};

module.exports = {
  email,
  password,
  verifyParams: [
    email,
    password
  ],
  userInstance,
  userNotVerified: {
    id: 2,
    email: 'notverified@email.com',
    role: 'ADMIN',
    verified: false
  },
  createParams: [ {
    user_id: 1,
    email,
    role: 'ADMIN'
  } ],
  sessionInstance: {
    token,
    email
  },
  response: {
    user: userInstance,
    session: {
      token,
      email
    }
  },
  commonError,
  notVerifiedError: {
    errors: [ {
      path: 'user',
      message: 'Not verified'
    } ]
  },
  wrongDataError: {
    errors: [ {
      path: 'email or password',
      message: 'email or password doesn\'t match'
    } ]
  }
};
