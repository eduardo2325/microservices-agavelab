const _ = require('lodash');

const user = {
  id: 1,
  first_name: 'A first name',
  last_name: 'With a last name',
  email: 'some@email.com',
  role: 'ADMIN',
  verified: true
};
const response = {
  id: 1,
  first_name: 'A first name',
  last_name: 'With a last name',
  email: 'some@email.com',
  role: 'ADMIN',
  verified: true
};

const userWithoutRole = _.cloneDeep(user);
const responseWithoutRole = _.cloneDeep(response);

delete userWithoutRole.role;
responseWithoutRole.role = '';

module.exports = {
  user,
  userWithoutRole,
  response,
  responseWithoutRole
};
