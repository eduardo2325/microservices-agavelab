const { errors } = require('../../../../common/fixtures');
const { commonError } = errors;

module.exports = {
  userId: 1,
  oldPassword: 'password',
  newPassword: 'new password',
  encrypted: '!@#$%^&*()_+',
  verifyParams: [
    null,
    'password',
    1
  ],
  user: {
    id: 1,
    name: 'User',
    email: 'user@email.com',
    role: 'ADMIN',
    save: () => {
      return;
    },
    getInfo: () => {
      return;
    }
  },
  commonError,
  wrongPasswordError: {
    errors: [ {
      path: 'old_password',
      message: 'old_password is incorrect'
    } ]
  }
};
