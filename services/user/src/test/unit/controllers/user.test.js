const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const log = require('/var/lib/core/js/log').prototype;
const controller = require('../../../api/controllers/user');
const errorHandler = require('../../../api/helpers/error');

const { User } = require('../../../models');
const { userProducer } = require('../../../api/producers/');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/User controller', () => {

  afterEach(() => {
    sandbox.restore();
  });

  describe('updatePassword', () => {
    const fixtures = require('./fixtures/users/update-password');
    const { guid, request, userInstance, cleanUser, logRequestParams,
       logResponseParams, updatePasswordParams, producerParams, error } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(User, 'updatePassword').callsFake(() => Promise.resolve(userInstance));
      sandbox.stub(userProducer, 'passwordUpdated').callsFake(() => Promise.resolve());
      sandbox.stub(userInstance, 'getRaw').callsFake(() => cleanUser);
      sandbox.stub(errorHandler, 'format').callsFake(() => Promise.reject());
    });

    it('should return an error if there was an with User.updatePassword', () => {
      User.updatePassword.restore();
      sandbox.stub(User, 'updatePassword').callsFake(() => Promise.reject(error));
      const callback = sandbox.spy();

      return controller.updatePassword(request, callback)
        .should.be.rejected
        .then(() => {

          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestParams);
          User.updatePassword.calledOnce.should.be.true;
          User.updatePassword.args[0].should.be.eql(updatePasswordParams);
          userProducer.passwordUpdated.called.should.be.false;
          userInstance.getRaw.called.should.be.false;
          errorHandler.format.args[0][0].should.be.eql(error);
          errorHandler.format.args[0][1].should.be.eql(guid);
          callback.called.should.be.false;
        });
    });

    it('should return an error if there was an with User.passwordUpdated', () => {
      userProducer.passwordUpdated.restore();
      sandbox.stub(userProducer, 'passwordUpdated').callsFake(() => Promise.reject(error));
      const callback = sandbox.spy();

      return controller.updatePassword(request, callback)
        .should.be.rejected
        .then(() => {

          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestParams);
          User.updatePassword.calledOnce.should.be.true;
          User.updatePassword.args[0].should.be.eql(updatePasswordParams);
          userProducer.passwordUpdated.calledOnce.should.be.true;
          userProducer.passwordUpdated.args[0].should.be.eql(producerParams);
          userInstance.getRaw.called.should.be.false;
          errorHandler.format.args[0][0].should.be.eql(error);
          errorHandler.format.args[0][1].should.be.eql(guid);
          callback.called.should.be.false;
        });
    });

    it('should return a success if there was no issue', () => {
      const callback = sandbox.spy();

      return controller.updatePassword(request, callback)
        .should.be.fulfilled
        .then(() => {

          log.message.calledTwice.should.be.true;
          log.message.args[0].should.be.eql(logRequestParams);
          User.updatePassword.calledOnce.should.be.true;
          User.updatePassword.args[0].should.be.eql(updatePasswordParams);
          userProducer.passwordUpdated.calledOnce.should.be.true;
          userProducer.passwordUpdated.args[0].should.be.eql(producerParams);
          userInstance.getRaw.calledOnce.should.be.true;
          log.message.args[1].should.be.eql(logResponseParams);
          errorHandler.format.called.should.be.false;
          callback.calledOnce.should.be.true;
          callback.args[0].should.be.eql([ null, cleanUser ]);
        });
    });
  });
});
