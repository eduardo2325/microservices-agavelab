const _ = require('lodash');
const { request, errors } = require('../../../../common/fixtures');
const { guid, requestType, responseType } = request;
const { commonError } = errors;

const grpcRequest = {
  guid,
  user_id: 1,
  old_password: 'old password',
  new_password: 'new password'
};
const user = {
  id: 1,
  first_name: 'First name',
  last_name: 'Last name',
  email: 'some@email.com',
  role: 'ADMIN',
  verified: true,
  getRaw: () => {
    return;
  }
};
const cleanUser = _.cloneDeep(user);

delete cleanUser.getRaw;

module.exports = {
  guid,
  request: {
    request: grpcRequest
  },
  userInstance: user,
  cleanUser,
  logRequestParams: [
    'Update password',
    grpcRequest,
    requestType,
    guid
  ],
  logResponseParams: [
    'Update password',
    cleanUser,
    responseType,
    guid
  ],
  updatePasswordParams: [
    1,
    'old password',
    'new password'
  ],
  producerParams: [
    user,
    guid
  ],
  error: commonError
};
