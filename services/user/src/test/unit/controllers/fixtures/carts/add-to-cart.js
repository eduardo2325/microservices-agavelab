const _ = require('lodash');
const { request, errors } = require('../../../../common/fixtures');
const { guid, requestType, responseType } = request;
const { commonError } = errors;

const grpcRequest = {
    guid,
    user_id: 1,
    product_id: 1,
    quantity: 2
};

const cart = {
    cart_id: 1,
    user_id: 1,
    created_at: new Date(),
    updated_at: new Date()
};
const cartItem = {
    cart_items_id: 1,
    product_id: 1,
    quantity: 2,
    cart_id: 1,
    created_at: new Date(),
    updated_at: new Date()
};
const summary = {
    products: [
        {
            name: 'Pants',
            quantity: 2,
            individualTotal: 10
        }
    ],
    totalQuantity: 2,
    totalPrice: 10
};
const cartItems = {
    cartItems: [cartItem]
}
const createOrUpdateCartParams = [
    _.cloneDeep(grpcRequest)
]

module.exports = {
  guid,
  request: {
    request: grpcRequest
  },
  cart,
  summary,
  cartItems,
  logRequestParams: [
    'Add product to cart',
    grpcRequest,
    requestType,
    guid
  ],
  createOrUpdateCartParams: createOrUpdateCartParams,
  error: commonError
};
