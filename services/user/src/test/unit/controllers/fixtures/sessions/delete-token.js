const { request, errors } = require('../../../../common/fixtures');
const { guid, token, requestType, responseType } = request;
const { commonError } = errors;

const grpcRequest = {
  guid,
  token
};

module.exports = {
  guid,
  request: {
    request: grpcRequest
  },
  logRequestParams: [
    'Delete token',
    grpcRequest,
    requestType,
    guid
  ],
  logResponseParams: [
    'Delete token',
    {},
    responseType,
    guid
  ],
  destroyParams: [ {
    where: {
      token
    }
  } ],
  error: commonError
};
