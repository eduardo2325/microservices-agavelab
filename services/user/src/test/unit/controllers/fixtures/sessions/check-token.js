const { request, errors } = require('../../../../common/fixtures');
const { guid, token, requestType, responseType } = request;
const { commonError } = errors;

const expirationDate = new Date();
const grpcRequest = {
  guid,
  token
};
const session = {
  id: 1,
  email: 'some@email.com',
  token,
  expiration_date: expirationDate,
  created_at: expirationDate,
  updated_at: expirationDate,
  deleted_at: expirationDate,
  get: () => {
    return;
  }
};
const response = {
  email: 'some@email.com',
  token,
  expiration_date: expirationDate.toISOString()
};

module.exports = {
  guid,
  request: {
    request: grpcRequest
  },
  session,
  response,
  logRequestParams: [
    'Check token',
    grpcRequest,
    requestType,
    guid
  ],
  logResponseParams: [
    'Check token',
    response,
    responseType,
    guid
  ],
  checkTokenParams: [ token ],
  error: commonError
};
