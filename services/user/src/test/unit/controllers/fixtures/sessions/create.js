const { request, errors } = require('../../../../common/fixtures');
const { guid, token, requestType, responseType } = request;
const { commonError } = errors;

const expirationDate = new Date();
const grpcRequest = {
  guid,
  email: 'some@email.com',
  password: 'some password'
};
const session = {
  session: {
    token,
    expiration_date: expirationDate
  },
  user: {
    id: 1,
    email: 'some@email.com',
    role: 'ADMIN'
  }
};
const response = {
  token,
  expiration_date: expirationDate.toISOString(),
  user_id: 1,
  email: 'some@email.com',
  role: 'ADMIN'
};

module.exports = {
  guid,
  request: {
    request: grpcRequest
  },
  session,
  response,
  logRequestParams: [
    'Session create',
    grpcRequest,
    requestType,
    guid
  ],
  logResponseParams: [
    'Session create',
    response,
    responseType,
    guid
  ],
  verifyAndCreateParams: [
    grpcRequest.email,
    grpcRequest.password
  ],
  error: commonError
};
