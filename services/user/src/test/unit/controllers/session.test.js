const _ = require('lodash');
const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const log = require('/var/lib/core/js/log').prototype;
const controller = require('../../../api/controllers/session');
const errorHandler = require('../../../api/helpers/error');

const { Session } = require('../../../models');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Session controller', () => {

  afterEach(() => {
    sandbox.restore();
  });

  describe('create', () => {
    const fixtures = require('./fixtures/sessions/create');
    const { guid, request, session, response, logRequestParams,
      logResponseParams, verifyAndCreateParams, error } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(Session, 'verifyAndCreate').callsFake(() => Promise.resolve(session));
      sandbox.stub(errorHandler, 'format').callsFake(() => Promise.reject());
    });

    it('should return an error if there was an with Session.verifyAndCreate', () => {
      Session.verifyAndCreate.restore();
      sandbox.stub(Session, 'verifyAndCreate').callsFake(() => Promise.reject(error));
      const callback = sandbox.spy();

      return controller.create(request, callback)
        .should.be.rejected
        .then(() => {

          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestParams);
          Session.verifyAndCreate.calledOnce.should.be.true;
          Session.verifyAndCreate.args[0].should.be.eql(verifyAndCreateParams);
          errorHandler.format.args[0][0].should.be.eql(error);
          errorHandler.format.args[0][1].should.be.eql(guid);
          callback.called.should.be.false;
        });
    });

    it('should return a success if there was no issue', () => {
      const callback = sandbox.spy();

      return controller.create(request, callback)
        .should.be.fulfilled
        .then(() => {

          log.message.calledTwice.should.be.true;
          log.message.args[0].should.be.eql(logRequestParams);
          Session.verifyAndCreate.calledOnce.should.be.true;
          Session.verifyAndCreate.args[0].should.be.eql(verifyAndCreateParams);
          log.message.args[1].should.be.eql(logResponseParams);
          errorHandler.format.called.should.be.false;
          callback.calledOnce.should.be.true;
          callback.args[0].should.be.eql([ null, response ]);
        });
    });
  });

  describe('deleteToken', () => {
    const fixtures = require('./fixtures/sessions/delete-token');
    const { guid, request, logRequestParams, logResponseParams, destroyParams, error } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(Session, 'destroy').callsFake(() => Promise.resolve());
      sandbox.stub(errorHandler, 'format').callsFake(() => Promise.reject());
    });

    it('should return an error if there was an with Session.destroy', () => {
      Session.destroy.restore();
      sandbox.stub(Session, 'destroy').callsFake(() => Promise.reject(error));
      const callback = sandbox.spy();

      return controller.deleteToken(request, callback)
        .should.be.rejected
        .then(() => {

          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestParams);
          Session.destroy.calledOnce.should.be.true;
          Session.destroy.args[0].should.be.eql(destroyParams);
          errorHandler.format.args[0][0].should.be.eql(error);
          errorHandler.format.args[0][1].should.be.eql(guid);
          callback.called.should.be.false;
        });
    });

    it('should return a success if there was no issue', () => {
      const callback = sandbox.spy();

      return controller.deleteToken(request, callback)
        .should.be.fulfilled
        .then(() => {

          log.message.calledTwice.should.be.true;
          log.message.args[0].should.be.eql(logRequestParams);
          Session.destroy.calledOnce.should.be.true;
          Session.destroy.args[0].should.be.eql(destroyParams);
          log.message.args[1].should.be.eql(logResponseParams);
          errorHandler.format.called.should.be.false;
          callback.calledOnce.should.be.true;
          callback.args[0].should.be.eql([ null, {} ]);
        });
    });
  });

  describe('checkToken', () => {
    const fixtures = require('./fixtures/sessions/check-token');
    const { guid, request, session, response, logRequestParams, logResponseParams,
      checkTokenParams, error } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(Session, 'checkToken').callsFake(() => Promise.resolve(session));
      sandbox.stub(errorHandler, 'format').callsFake(() => Promise.reject());
      sandbox.stub(session, 'get').callsFake(() => {
        const data = _.cloneDeep(session);

        delete data.get;

        return data;
      });
    });

    it('should return an error if there was an with Session.checkToken', () => {
      Session.checkToken.restore();
      sandbox.stub(Session, 'checkToken').callsFake(() => Promise.reject(error));
      const callback = sandbox.spy();

      return controller.checkToken(request, callback)
        .should.be.rejected
        .then(() => {

          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestParams);
          Session.checkToken.calledOnce.should.be.true;
          Session.checkToken.args[0].should.be.eql(checkTokenParams);
          errorHandler.format.args[0][0].should.be.eql(error);
          errorHandler.format.args[0][1].should.be.eql(guid);
          callback.called.should.be.false;
        });
    });

    it('should return a success if there was no issue', () => {
      const callback = sandbox.spy();

      return controller.checkToken(request, callback)
        .should.be.fulfilled
        .then(() => {

          log.message.calledTwice.should.be.true;
          log.message.args[0].should.be.eql(logRequestParams);
          Session.checkToken.calledOnce.should.be.true;
          Session.checkToken.args[0].should.be.eql(checkTokenParams);
          log.message.args[1].should.be.eql(logResponseParams);
          errorHandler.format.called.should.be.false;
          callback.calledOnce.should.be.true;
          callback.args[0].should.be.eql([ null, response ]);
        });
    });
  });
});
