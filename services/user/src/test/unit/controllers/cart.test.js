const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const log = require('/var/lib/core/js/log').prototype;
const controller = require('../../../api/controllers/cart');
const errorHandler = require('../../../api/helpers/error');

const { Cart } = require('../../../models');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Cart controller', () => {

  afterEach(() => {
    sandbox.restore();
  });

  describe('addToCart', () => {
    const fixtures = require('./fixtures/carts/add-to-cart');
    const { guid, request, error, logRequestParams, cart, summary, cartItems, createOrUpdateCartParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').returns(true);
      sandbox.stub(Cart, 'createOrUpdateCart').resolves(summary);
      sandbox.stub(errorHandler, 'format').rejects();
    });

    it('should return a success if there was no issue', () => {
      const callback = sandbox.spy();

      return controller.addToCart(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.args[0].should.be.eql(logRequestParams);
          Cart.createOrUpdateCart.calledOnce.should.be.true;
          Cart.createOrUpdateCart.args[0].should.be.eql(createOrUpdateCartParams);
          callback.calledOnce.should.be.true;
          callback.args[0].should.be.eql([ null, summary ]);
        });
    });
  });
});
