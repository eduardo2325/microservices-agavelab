const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const log = require('/var/lib/core/js/log').prototype;
const helper = require('../../../api/helpers/error');
const helperFixtures = require('./fixtures/error');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Error helper', () => {

  afterEach(() => {
    sandbox.restore();
  });

  describe('format', () => {

    const fixtures = helperFixtures.format;
    const { err, errMessage, errorFormatted, guid } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return helper.format(errMessage, guid, callback)
        .should.be.fulfilled
        .then(() => {

          const errorMessage = callback.args[0][0].message;

          log.error.calledOnce.should.be.true;
          log.error.calledWithMatch(errMessage, guid).should.be.true;
          callback.calledOnce.should.be.true;
          errorMessage.should.be.eql(errorFormatted);
        });
    });

    it('should return a successful response with sequelize error', () => {
      const callback = sandbox.spy();
      const stringifiedError = JSON.stringify(err.errors[0]);

      return helper.format(err, guid, callback)
        .should.be.fulfilled
        .then(() => {

          const errorMessage = callback.args[0][0].message;

          log.error.calledOnce.should.be.true;
          log.error.calledWithMatch(err, guid).should.be.true;
          callback.calledOnce.should.be.true;
          errorMessage.should.be.eql(stringifiedError);
        });
    });
  });
});

