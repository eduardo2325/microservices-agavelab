const date = new Date();

module.exports = {
  expirationTime: {
    date,
    timeToAdd: 30
  }
};
