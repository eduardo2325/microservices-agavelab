const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const helper = require('../../../api/helpers/utils');
const helperFixtures = require('./fixtures/utils');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Error helper', () => {

  afterEach(() => {
    sandbox.restore();
  });

  describe('expirationTime', () => {

    const fixtures = helperFixtures.expirationTime;
    const { date, timeToAdd } = fixtures;
    let clock;

    before(() => {
      clock = sinon.useFakeTimers(date.getTime());
    });

    after(() => {
      clock.restore();
    });

    it('should return a successful response with sequelize error', () => {
      const expirationDate = helper.expirationTime(timeToAdd);
      let timeDiff = Math.abs(expirationDate - date);

      timeDiff = Math.floor(timeDiff / 1000 / 60);
      timeDiff.should.be.eql(timeToAdd);
    });
  });
});
