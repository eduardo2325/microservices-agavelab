const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const helper = require('../../../api/helpers/crypto');
const helperFixtures = require('./fixtures/crypto');
const bcrypt = require('bcryptjs');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Error helper', () => {

  afterEach(() => {
    sandbox.restore();
  });

  describe('encrypt', () => {

    const fixtures = helperFixtures.encrypt;
    const { value, hash, salt } = fixtures;

    before(() => {
      sandbox.stub(bcrypt, 'genSaltSync').callsFake(() => salt);
      sandbox.stub(bcrypt, 'hashSync').callsFake(() => hash);
    });

    it('should return a successful response', () => {
      const result = helper.encrypt(value);

      bcrypt.genSaltSync.calledOnce.should.be.true;
      bcrypt.genSaltSync.calledWith(10).should.be.true;
      bcrypt.hashSync.calledOnce.should.be.true;
      result.should.be.eql(hash);
    });
  });
});
